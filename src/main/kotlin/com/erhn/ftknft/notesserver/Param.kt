package com.erhn.ftknft.notesserver

object Param {
    const val BOARD_ID = "board_id"
    const val TASK_ID = "task_id"
    const val INVITE_ID = "invite_id"
}
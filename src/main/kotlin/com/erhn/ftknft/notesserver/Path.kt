package com.erhn.ftknft.notesserver

object Path {
    const val AUTH = "auth"
    const val REGISTER = "register"
    const val REFRESH = "refresh"
    const val BOARDS = "boards"
    const val INVITES = "invites"
    const val TASKS = "tasks"

    const val ACCEPT = "accept"
    const val DECLINE = "decline"
    const val CANCEL = "cancel"
}
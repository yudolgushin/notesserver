package com.erhn.ftknft.notesserver.presentation.model

import com.erhn.ftknft.notesserver.presentation.JsonField
import com.google.gson.annotations.SerializedName

data class NewtTask(
    @SerializedName(JsonField.Task.TITLE)
    val title: String,
    @SerializedName(JsonField.Task.IS_COMPLETE)
    val isComplete: Boolean?
) : Verifaiable {

    override fun verify() {
        title!!
    }
}
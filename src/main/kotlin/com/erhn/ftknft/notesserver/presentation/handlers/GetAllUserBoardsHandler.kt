package com.erhn.ftknft.notesserver.presentation.handlers

import com.erhn.ftknft.notesserver.domain.services.checktoken.CheckTokenService
import com.erhn.ftknft.notesserver.domain.services.getuserboards.GetUserBoardsService
import com.erhn.ftknft.notesserver.presentation.handlers.base.AuthHeaderHandler
import com.erhn.ftknft.notesserver.utils.jwt.getUID
import com.erhn.ftknft.notesserver.utils.jwt.model.Jwt
import com.google.gson.Gson
import io.vertx.ext.web.RoutingContext

class GetAllUserBoardsHandler(gson: Gson, checkTokenService: CheckTokenService, private val getUserBoardsService: GetUserBoardsService) :
    AuthHeaderHandler(gson, checkTokenService) {

    override fun onTokenSuccess(jwt: Jwt, event: RoutingContext) {
        val uid = jwt.getUID()
        val list = getUserBoardsService.getAllUserBoards(uid)
        event.endResponse(list)
    }
}
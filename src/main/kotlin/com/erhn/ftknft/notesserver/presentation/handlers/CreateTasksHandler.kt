package com.erhn.ftknft.notesserver.presentation.handlers

import com.erhn.ftknft.notesserver.Param
import com.erhn.ftknft.notesserver.domain.services.checktoken.CheckTokenService
import com.erhn.ftknft.notesserver.domain.services.checkuserboard.CheckBoardUserService
import com.erhn.ftknft.notesserver.domain.services.createtasks.CreateTasksService
import com.erhn.ftknft.notesserver.presentation.handlers.base.BaseAuthBodyHandler
import com.erhn.ftknft.notesserver.presentation.model.CreateTasksRequest
import com.erhn.ftknft.notesserver.presentation.model.TaskResponse
import com.erhn.ftknft.notesserver.presentation.model.TasksResponse
import com.erhn.ftknft.notesserver.utils.jwt.getUID
import com.erhn.ftknft.notesserver.utils.jwt.model.Jwt
import com.google.gson.Gson
import io.vertx.ext.web.RoutingContext

class CreateTasksHandler(
    gson: Gson,
    checkTokenService: CheckTokenService,
    private val boardUserService: CheckBoardUserService,
    private val createTasksService: CreateTasksService
) :
    BaseAuthBodyHandler<CreateTasksRequest>(gson, checkTokenService, CreateTasksRequest::class.java) {

    override fun onBodyParseSuccess(tokenJwt: Jwt, body: CreateTasksRequest, event: RoutingContext) {
        val boardID = event.request().getParam(Param.BOARD_ID)!!
        val userId = tokenJwt.getUID()
        val role = boardUserService.checkUserRole(boardID, userId)
        val createdTasks = createTasksService.createTasks(boardID, userId, body.tasks).map {
            TaskResponse(
                id = it.id,
                ownerId = it.ownerId,
                title = it.title,
                isComplete = it.isComplete,
                createdDate = it.createdDate
            )
        }
        event.endResponse(TasksResponse(createdTasks))
    }
}
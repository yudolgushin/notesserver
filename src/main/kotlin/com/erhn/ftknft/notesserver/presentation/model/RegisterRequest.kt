package com.erhn.ftknft.notesserver.presentation.model

import com.erhn.ftknft.notesserver.presentation.JsonField
import com.google.gson.annotations.SerializedName

data class RegisterRequest(
    @SerializedName(JsonField.LOGIN)
    val login: String,
    @SerializedName(JsonField.PASSWORD)
    val password: String
) : Verifaiable {
    override fun verify() {
        login!!
        password!!
    }
}
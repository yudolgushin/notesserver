package com.erhn.ftknft.notesserver.presentation.model

import com.erhn.ftknft.notesserver.presentation.JsonField
import com.google.gson.annotations.SerializedName

data class UpdateTaskRequest(
    @SerializedName(JsonField.Task.ID)
    val id: String,
    @SerializedName(JsonField.Task.TITLE)
    val title: String?,
    @SerializedName(JsonField.Task.IS_COMPLETE)
    val isComplete: Boolean?
) : Verifaiable {
    override fun verify() {
        id!!
    }
}
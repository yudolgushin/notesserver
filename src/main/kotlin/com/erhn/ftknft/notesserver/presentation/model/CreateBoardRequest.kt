package com.erhn.ftknft.notesserver.presentation.model

import com.erhn.ftknft.notesserver.presentation.JsonField
import com.google.gson.annotations.SerializedName

data class CreateBoardRequest(
    @SerializedName(JsonField.Board.TITLE)
    val title: String
) : Verifaiable {
    override fun verify() {
        title!!
    }
}
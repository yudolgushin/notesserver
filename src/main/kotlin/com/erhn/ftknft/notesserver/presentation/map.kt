package com.erhn.ftknft.notesserver.presentation

import com.erhn.ftknft.notesserver.data.dto.BoardDTO
import com.erhn.ftknft.notesserver.data.dto.TaskDTO
import com.erhn.ftknft.notesserver.presentation.model.BoardResponse
import com.erhn.ftknft.notesserver.presentation.model.TaskResponse

fun BoardDTO.toBoardResponse(tasks: List<TaskResponse>) = BoardResponse(
    id = id,
    ownerId = ownerId,
    title = title,
    createDate = createDate,
    updateDate = updateDate,
    tasks = tasks
)


fun TaskDTO.toTaskResponse() = TaskResponse(
    id = id,
    ownerId = ownerId,
    title = title,
    isComplete = isComplete,
    createdDate = createdDate
)
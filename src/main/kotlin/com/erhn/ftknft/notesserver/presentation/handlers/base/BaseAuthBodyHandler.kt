package com.erhn.ftknft.notesserver.presentation.handlers.base

import com.erhn.ftknft.notesserver.domain.services.checktoken.CheckTokenService
import com.erhn.ftknft.notesserver.presentation.model.Verifaiable
import com.erhn.ftknft.notesserver.utils.jwt.model.Jwt
import com.google.gson.Gson
import io.vertx.ext.web.RoutingContext

abstract class BaseAuthBodyHandler<T : Verifaiable>(
    gson: Gson,
    checkTokenService: CheckTokenService,
    private val clazz: Class<T>
) : AuthHeaderHandler(gson, checkTokenService) {

    final override fun onTokenSuccess(jwt: Jwt, event: RoutingContext) {
        val requestBody = try {
            gson.fromJson(event.bodyAsString, clazz).apply { tryVerify() }
        } catch (e: Throwable) {
            event.invalidRequestBody()
            return
        }
        onBodyParseSuccess(jwt, requestBody, event)
    }

    abstract fun onBodyParseSuccess(tokenJwt: Jwt, body: T, event: RoutingContext)

}
package com.erhn.ftknft.notesserver.presentation.handlers

import com.erhn.ftknft.notesserver.domain.services.createtokens.CreateTokensService
import com.erhn.ftknft.notesserver.domain.services.refreshtokens.FindRefreshTokenService
import com.erhn.ftknft.notesserver.presentation.handlers.base.BaseBodyHandler
import com.erhn.ftknft.notesserver.presentation.model.RefreshRequest
import com.erhn.ftknft.notesserver.presentation.model.TokensResponse
import com.google.gson.Gson
import io.vertx.ext.web.RoutingContext

class RefreshTokensHandler(
    gson: Gson,
    private val findRefreshTokenService: FindRefreshTokenService,
    private val createTokensService: CreateTokensService
) :
    BaseBodyHandler<RefreshRequest>(gson, RefreshRequest::class.java) {

    override fun onBodyParseSuccess(body: RefreshRequest, event: RoutingContext) {
        val user = findRefreshTokenService.findUserByRefreshToken(body.refreshToken)
        val tokens = createTokensService.createTokensForUser(user)
        event.endResponse(TokensResponse(tokens.first, tokens.second))
    }
}
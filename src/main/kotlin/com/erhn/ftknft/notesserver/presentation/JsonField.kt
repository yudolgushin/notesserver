package com.erhn.ftknft.notesserver.presentation

object JsonField {

    const val LOGIN = "login"
    const val PASSWORD = "password"
    const val ACCESS_TOKEN = "access_token"
    const val REFRESH_TOKEN = "refresh_token"
    const val INTERNAL_ERROR_CODE = "internal_code"
    const val ERROR_MESSAGE = "message"

    object Board {
        const val ID = "id"
        const val OWNER_ID = "owner_Id"
        const val TITLE = "title"
        const val CREATE_DATE = "create_date"
        const val UPDATE_DATE = "update_date"
        const val TASKS = "tasks"
    }

    object Task {
        const val ID = "id"
        const val BOARD_ID = "board_id"
        const val OWNER_ID = "owner_id"
        const val TITLE = "title"
        const val IS_COMPLETE = "is_complete"
        const val CREATE_DATE = "create_date"
    }

    object Invite {
        const val GUEST_ID = "guest_id"
        const val OWNER_ID = "owner_id"
        const val OWNER_LOGIN = "owner_login"
    }
}
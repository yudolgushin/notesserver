package com.erhn.ftknft.notesserver.presentation.handlers

import com.erhn.ftknft.notesserver.domain.services.checktoken.CheckTokenService
import com.erhn.ftknft.notesserver.domain.services.createboard.CreateBoardService
import com.erhn.ftknft.notesserver.presentation.handlers.base.BaseAuthBodyHandler
import com.erhn.ftknft.notesserver.presentation.model.CreateBoardRequest
import com.erhn.ftknft.notesserver.presentation.toBoardResponse
import com.erhn.ftknft.notesserver.utils.jwt.getUID
import com.erhn.ftknft.notesserver.utils.jwt.model.Jwt
import com.google.gson.Gson
import io.vertx.ext.web.RoutingContext

class CreateBoardHandler(gson: Gson, checkTokenService: CheckTokenService, private val createBoardService: CreateBoardService) :

    BaseAuthBodyHandler<CreateBoardRequest>(gson, checkTokenService, CreateBoardRequest::class.java) {

    override fun onBodyParseSuccess(tokenJwt: Jwt, body: CreateBoardRequest, event: RoutingContext) {
        val userId = tokenJwt.getUID()
        val createdBoard = createBoardService.createBoard(userId, body.title)
        event.endResponse(createdBoard.toBoardResponse(listOf()))
    }
}
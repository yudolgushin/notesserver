package com.erhn.ftknft.notesserver.presentation.model

import com.erhn.ftknft.notesserver.presentation.JsonField
import com.google.gson.annotations.SerializedName

data class RefreshRequest(
    @SerializedName(JsonField.REFRESH_TOKEN)
    val refreshToken: String
) : Verifaiable {
    override fun verify() {
        refreshToken!!
    }
}
package com.erhn.ftknft.notesserver.presentation.model

import com.erhn.ftknft.notesserver.presentation.JsonField
import com.google.gson.annotations.SerializedName

data class CreateBoardResponse(
    @SerializedName(JsonField.Board.ID)
    val id: String,
    @SerializedName(JsonField.Board.TITLE)
    val title: String,
    @SerializedName(JsonField.Board.CREATE_DATE)
    val createDate: String
)
package com.erhn.ftknft.notesserver.presentation.model

import com.erhn.ftknft.notesserver.presentation.JsonField
import com.google.gson.annotations.SerializedName

data class AddToBoardRequest(
    @SerializedName(JsonField.Invite.GUEST_ID)
    val guestId: String
) : Verifaiable {

    override fun verify() {
        guestId!!
    }
}
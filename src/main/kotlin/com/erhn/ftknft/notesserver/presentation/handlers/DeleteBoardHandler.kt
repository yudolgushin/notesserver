package com.erhn.ftknft.notesserver.presentation.handlers

import com.erhn.ftknft.notesserver.Param
import com.erhn.ftknft.notesserver.domain.services.checktoken.CheckTokenService
import com.erhn.ftknft.notesserver.domain.services.deletebooard.DeleteBoardService
import com.erhn.ftknft.notesserver.presentation.handlers.base.AuthHeaderHandler
import com.erhn.ftknft.notesserver.utils.jwt.getUID
import com.erhn.ftknft.notesserver.utils.jwt.model.Jwt
import com.google.gson.Gson
import io.vertx.ext.web.RoutingContext

class DeleteBoardHandler(gson: Gson, checkTokenService: CheckTokenService, private val deleteBoardService: DeleteBoardService) :
    AuthHeaderHandler(gson, checkTokenService) {

    override fun onTokenSuccess(jwt: Jwt, event: RoutingContext) {
        val boardID = event.request().getParam(Param.BOARD_ID)!!
        deleteBoardService.delete(jwt.getUID(), boardID)
        event.ok()
    }
}
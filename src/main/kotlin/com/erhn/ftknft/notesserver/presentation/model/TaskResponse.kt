package com.erhn.ftknft.notesserver.presentation.model

import com.erhn.ftknft.notesserver.presentation.JsonField
import com.google.gson.annotations.SerializedName

data class TaskResponse(
    @SerializedName(JsonField.Task.ID)
    val id: String,
    @SerializedName(JsonField.Task.OWNER_ID)
    val ownerId: String,
    @SerializedName(JsonField.Task.TITLE)
    val title: String,
    @SerializedName(JsonField.Task.IS_COMPLETE)
    val isComplete: Boolean,
    @SerializedName(JsonField.Task.CREATE_DATE)
    val createdDate: String
)
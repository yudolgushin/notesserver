package com.erhn.ftknft.notesserver.presentation.model

import com.erhn.ftknft.notesserver.presentation.JsonField
import com.google.gson.annotations.SerializedName

data class UpdateBoardRequest(
    @SerializedName(JsonField.Board.TITLE)
    val title: String?,
    @SerializedName(JsonField.Board.TASKS)
    val tasks: List<UpdateTaskRequest>?
) : Verifaiable {

    override fun verify() {
    }
}
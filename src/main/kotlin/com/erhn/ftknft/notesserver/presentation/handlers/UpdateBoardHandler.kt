package com.erhn.ftknft.notesserver.presentation.handlers

import com.erhn.ftknft.notesserver.Param
import com.erhn.ftknft.notesserver.domain.services.checktoken.CheckTokenService
import com.erhn.ftknft.notesserver.domain.services.updateboard.UpdateBoardService
import com.erhn.ftknft.notesserver.presentation.handlers.base.BaseAuthBodyHandler
import com.erhn.ftknft.notesserver.presentation.model.UpdateBoardRequest
import com.erhn.ftknft.notesserver.utils.jwt.getUID
import com.erhn.ftknft.notesserver.utils.jwt.model.Jwt
import com.google.gson.Gson
import io.vertx.ext.web.RoutingContext

class UpdateBoardHandler(gson: Gson, checkTokenService: CheckTokenService, private val updateBoardService: UpdateBoardService) :
    BaseAuthBodyHandler<UpdateBoardRequest>(gson, checkTokenService, UpdateBoardRequest::class.java) {

    override fun onBodyParseSuccess(tokenJwt: Jwt, body: UpdateBoardRequest, event: RoutingContext) {
        val uid = tokenJwt.getUID()
        val boardId = event.request().getParam(Param.BOARD_ID)!!
        val response = updateBoardService.updateBoard(uid, boardId, body)
        event.endResponse(response)
    }
}
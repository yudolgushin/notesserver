package com.erhn.ftknft.notesserver.presentation.handlers

import com.erhn.ftknft.notesserver.Param
import com.erhn.ftknft.notesserver.domain.model.InviteDomain
import com.erhn.ftknft.notesserver.domain.services.checktoken.CheckTokenService
import com.erhn.ftknft.notesserver.domain.services.inviteservice.InviteService
import com.erhn.ftknft.notesserver.presentation.handlers.base.BaseAuthBodyHandler
import com.erhn.ftknft.notesserver.presentation.model.AddToBoardRequest
import com.erhn.ftknft.notesserver.utils.jwt.getUID
import com.erhn.ftknft.notesserver.utils.jwt.model.Jwt
import com.google.gson.Gson
import io.vertx.ext.web.RoutingContext

class AddToBoardHandler(gson: Gson, checkTokenService: CheckTokenService, private val inviteService: InviteService) :
    BaseAuthBodyHandler<AddToBoardRequest>(gson, checkTokenService, AddToBoardRequest::class.java) {

    override fun onBodyParseSuccess(tokenJwt: Jwt, body: AddToBoardRequest, event: RoutingContext) {
        val uid = tokenJwt.getUID()
        val boardId = event.request().getParam(Param.BOARD_ID)!!
        inviteService.inviteUser(InviteDomain(boardId, uid, body.guestId))
        event.ok()
    }
}
package com.erhn.ftknft.notesserver.presentation.model

import com.erhn.ftknft.notesserver.presentation.JsonField
import com.google.gson.annotations.SerializedName

data class TokensResponse(
    @SerializedName(JsonField.ACCESS_TOKEN)
    val accessToken: String,
    @SerializedName(JsonField.REFRESH_TOKEN)
    val refreshToken: String
)
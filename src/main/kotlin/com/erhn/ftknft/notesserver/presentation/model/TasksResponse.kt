package com.erhn.ftknft.notesserver.presentation.model

import com.erhn.ftknft.notesserver.presentation.JsonField
import com.google.gson.annotations.SerializedName

data class TasksResponse(
    @SerializedName(JsonField.Board.TASKS)
    val tasks: List<TaskResponse>
)
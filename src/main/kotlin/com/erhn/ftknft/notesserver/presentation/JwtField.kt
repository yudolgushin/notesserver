package com.erhn.ftknft.notesserver.presentation

object JwtField {
    const val UID = "uid"
    const val ROLE = "role"
    const val ET = "et"
    const val SID = "sid"

    enum class Role(val value: String) {
        DEFAULT("default"), ADMIN("admin")
    }
}
package com.erhn.ftknft.notesserver.presentation.handlers.base

import com.erhn.ftknft.notesserver.Header
import com.erhn.ftknft.notesserver.domain.exceptions.BaseException
import com.erhn.ftknft.notesserver.presentation.StatusCode
import com.erhn.ftknft.notesserver.presentation.model.error.ErrorResponse
import com.erhn.ftknft.notesserver.presentation.model.error.InternalCode
import com.erhn.ftknft.notesserver.utils.Logger
import com.google.gson.Gson
import io.vertx.core.Handler
import io.vertx.ext.web.RoutingContext

abstract class BaseHandler(protected val gson: Gson) : Handler<RoutingContext> {

    val REQUEST = "request"
    val RESPONSE = "response"

    final override fun handle(event: RoutingContext) {
        try {
            println()
            Logger.log(REQUEST, "<${event.request().method()}${event.request().uri()}")
            Logger.log(REQUEST, event.bodyAsString)
            val headers = event.request().headers()
            for (header in headers) {
                Logger.log(REQUEST, "${header.key}: ${header.value}")
            }
            innerHandle(event)
        } catch (e: Throwable) {
            event.error(e)
        }
    }

    abstract fun innerHandle(event: RoutingContext)

    protected fun <T> RoutingContext.endResponse(body: T, code: Int = StatusCode.OK) {
        val response = request().response()
        if (response.ended()) return
        val respObj = if (body is String) {
            body
        } else {
            gson.toJson(body)
        }
        Logger.log(RESPONSE, ">${request().uri()}")
        Logger.log(RESPONSE, code.toString())
        Logger.log(RESPONSE, respObj)
        response.setStatusCode(code)
        response.putHeader(Header.CONTENT_TYPE, Header.APPLICATION_JSON)
        if (respObj.isBlank() || respObj == "{}") {
            response.end()
        } else {
            response.end(respObj)
        }

    }


    protected fun RoutingContext.ok() {
        endResponse("")
    }

    protected fun RoutingContext.invalidRequestBody() {
        endResponse("Invalid request body", StatusCode.BAD_REQUEST)
    }

    protected fun RoutingContext.error(throwable: Throwable) {
        if (throwable !is BaseException) {
            endResponse(ErrorResponse(message = throwable.message, internalCode = InternalCode.UNKNOWN), StatusCode.INNER)
        } else {
            val type = throwable.type
            endResponse(ErrorResponse(type.internalCode, type.message), type.httpCode)

        }

    }

    protected fun RoutingContext.unauthorized() {
        endResponse("", StatusCode.UNAUTHORIZED)
    }

}
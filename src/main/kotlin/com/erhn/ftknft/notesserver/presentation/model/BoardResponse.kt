package com.erhn.ftknft.notesserver.presentation.model

import com.erhn.ftknft.notesserver.presentation.JsonField
import com.google.gson.annotations.SerializedName

data class BoardResponse(
    @SerializedName(JsonField.Board.ID)
    val id: String,
    @SerializedName(JsonField.Board.OWNER_ID)
    val ownerId: String,
    @SerializedName(JsonField.Board.TITLE)
    val title: String,
    @SerializedName(JsonField.Board.CREATE_DATE)
    val createDate: String,
    @SerializedName(JsonField.Board.UPDATE_DATE)
    val updateDate: String,
    @SerializedName(JsonField.Board.TASKS)
    val tasks: List<TaskResponse>
)
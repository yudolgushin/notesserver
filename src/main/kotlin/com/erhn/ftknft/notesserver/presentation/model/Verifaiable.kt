package com.erhn.ftknft.notesserver.presentation.model

import com.erhn.ftknft.notesserver.domain.exceptions.BaseException

interface Verifaiable {

    fun tryVerify() {
        try {
            verify()
        } catch (t: Throwable) {
            throw BaseException(BaseException.Type.BAD_REQUEST)
        }
    }

    fun verify()
}
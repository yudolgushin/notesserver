package com.erhn.ftknft.notesserver.presentation.model

import com.erhn.ftknft.notesserver.presentation.JsonField
import com.google.gson.annotations.SerializedName

data class CreateTasksRequest(
    @SerializedName(JsonField.Board.TASKS)
    val tasks: List<NewtTask>
) : Verifaiable {
    override fun verify() {
        tasks!!
    }
}
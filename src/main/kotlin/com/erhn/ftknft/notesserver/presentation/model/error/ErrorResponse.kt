package com.erhn.ftknft.notesserver.presentation.model.error

import com.erhn.ftknft.notesserver.presentation.JsonField
import com.google.gson.annotations.SerializedName

data class ErrorResponse(
    @SerializedName(JsonField.INTERNAL_ERROR_CODE)
    val internalCode: Int?,
    @SerializedName(JsonField.ERROR_MESSAGE)
    val message: String?
)
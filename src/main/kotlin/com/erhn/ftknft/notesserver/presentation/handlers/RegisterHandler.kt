package com.erhn.ftknft.notesserver.presentation.handlers

import com.erhn.ftknft.notesserver.domain.services.createtokens.CreateTokensService
import com.erhn.ftknft.notesserver.domain.services.createuser.CreateUserService
import com.erhn.ftknft.notesserver.presentation.handlers.base.BaseBodyHandler
import com.erhn.ftknft.notesserver.presentation.model.RegisterRequest
import com.erhn.ftknft.notesserver.presentation.model.TokensResponse
import com.google.gson.Gson
import io.vertx.ext.web.RoutingContext

class RegisterHandler(
    gson: Gson,
    private val createUserService: CreateUserService,
    protected val createTokensService: CreateTokensService
) :
    BaseBodyHandler<RegisterRequest>(gson, RegisterRequest::class.java) {


    override fun onBodyParseSuccess(body: RegisterRequest, event: RoutingContext) {
        val user = createUserService.register(body.login, body.password)
        val tokens = createTokensService.createTokensForUser(user)
        event.endResponse(TokensResponse(tokens.first, tokens.second))
    }

}
package com.erhn.ftknft.notesserver.presentation.handlers.base

import com.erhn.ftknft.notesserver.presentation.model.Verifaiable
import com.google.gson.Gson
import io.vertx.ext.web.RoutingContext

abstract class BaseBodyHandler<T : Verifaiable>(gson: Gson, private val clazz: Class<T>) : BaseHandler(gson) {

    override fun innerHandle(event: RoutingContext) {
        val requestBody = try {
            gson.fromJson(event.bodyAsString, clazz).apply { tryVerify() }

        } catch (e: Throwable) {
            event.invalidRequestBody()
            return
        }
        onBodyParseSuccess(requestBody, event)
    }

    abstract fun onBodyParseSuccess(body: T, event: RoutingContext)

}
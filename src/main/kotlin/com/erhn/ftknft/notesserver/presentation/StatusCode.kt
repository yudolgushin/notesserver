package com.erhn.ftknft.notesserver.presentation

object StatusCode {

    const val OK = 200
    const val BAD_REQUEST = 400
    const val UNAUTHORIZED = 401
    const val FORBIDDEN = 403
    const val NOT_FOUND = 404
    const val INNER = 500
}
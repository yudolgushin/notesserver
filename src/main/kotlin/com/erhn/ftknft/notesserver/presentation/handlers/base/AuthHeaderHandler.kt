package com.erhn.ftknft.notesserver.presentation.handlers.base

import com.erhn.ftknft.notesserver.domain.exceptions.BaseException
import com.erhn.ftknft.notesserver.domain.services.checktoken.CheckTokenService
import com.erhn.ftknft.notesserver.utils.jwt.model.Jwt
import com.google.gson.Gson
import io.vertx.ext.web.RoutingContext

abstract class AuthHeaderHandler(gson: Gson, private val checkTokenService: CheckTokenService) : BaseHandler(gson) {

    override fun innerHandle(event: RoutingContext) {
        val token = event.request().getHeader("Authorization")
        if (token == null) {
            throw BaseException(BaseException.Type.UNAUTHORIZED)
        }
        val jwt = checkTokenService.checkToken(token)
        onTokenSuccess(jwt, event)
    }

    abstract fun onTokenSuccess(jwt: Jwt, event: RoutingContext)
}
package com.erhn.ftknft.notesserver.presentation.model.error

object InternalCode {
    const val UNKNOWN = 99999

    //auth
    const val LOGIN_ALREADY_USED = 10000
    const val USER_NOT_EXISTS = 10001

    const val INVALID_LOGIN_LENGTH = 10020
    const val INVALID_PASSWORD_LENGTH = 10021

    //BOARDS
    const val BOARDS_NOT_EXISTS = 20001

}
package com.erhn.ftknft.notesserver.presentation.handlers

import com.erhn.ftknft.notesserver.Param
import com.erhn.ftknft.notesserver.domain.services.checktoken.CheckTokenService
import com.erhn.ftknft.notesserver.domain.services.deletebordertasks.DeleteBoardTasks
import com.erhn.ftknft.notesserver.presentation.handlers.base.AuthHeaderHandler
import com.erhn.ftknft.notesserver.utils.jwt.getUID
import com.erhn.ftknft.notesserver.utils.jwt.model.Jwt
import com.google.gson.Gson
import io.vertx.ext.web.RoutingContext

class DeleteBoardTasksHandler(gson: Gson, checkTokenService: CheckTokenService, private val deleteBoardTasks: DeleteBoardTasks) :
    AuthHeaderHandler(gson, checkTokenService) {

    override fun onTokenSuccess(jwt: Jwt, event: RoutingContext) {
        val uid = jwt.getUID()
        val boardId = event.request().getParam(Param.BOARD_ID)
        deleteBoardTasks.delete(uid, boardId)
        event.endResponse("")
    }
}
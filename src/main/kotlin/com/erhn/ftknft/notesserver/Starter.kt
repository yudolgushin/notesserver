package com.erhn.ftknft.notesserver

import io.vertx.core.Vertx
import org.koin.core.KoinComponent
import org.koin.core.context.startKoin

object Starter : KoinComponent {
    @JvmStatic
    fun main(args: Array<String>) {

        startKoin {
            modules(listOf(utilsModule, dataModule, domainModule, controlersModule))
        }
        Vertx.vertx().deployVerticle(MainVertical())

    }
}
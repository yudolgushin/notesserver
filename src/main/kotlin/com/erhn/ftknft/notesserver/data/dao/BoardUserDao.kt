package com.erhn.ftknft.notesserver.data.dao

import com.erhn.ftknft.notesserver.data.Db
import com.erhn.ftknft.notesserver.data.dto.BoardUserDTO
import com.erhn.ftknft.notesserver.utils.querybuilder.CreateTable
import com.erhn.ftknft.notesserver.utils.querybuilder.Delete
import com.erhn.ftknft.notesserver.utils.querybuilder.Insert
import com.erhn.ftknft.notesserver.utils.querybuilder.core.CD
import com.erhn.ftknft.notesserver.utils.querybuilder.core.IF_NOT_EXISTS
import com.erhn.ftknft.notesserver.utils.querybuilder.core.columnconstraint.Type
import java.sql.Connection
import java.sql.ResultSet

class BoardUserDao(conn: Connection) : BaseDao<BoardUserDTO>(conn) {

    override fun tableName(): String = Db.BoardsUsers.TABLE_NAME

    override fun createTableQuery(): String =
        CreateTable(tableName(), IF_NOT_EXISTS)
            .iniColumns()
            .column(Db.BoardsUsers.BOARD_ID, Type.TEXT)
            .column(Db.BoardsUsers.USER_ID, Type.TEXT)
            .column(Db.BoardsUsers.IS_USER_OWNER, Type.INTEGER)
            .foreignKey(Db.BoardsUsers.BOARD_ID).references(Db.Boards.TABLE_NAME, Db.Boards.ID).onDeleteCascade()
            .foreignKey(Db.BoardsUsers.USER_ID).references(Db.Users.TABLE_NAME, Db.Users.ID).onDeleteCascade()
            .endInits()
            .build()

    override fun insert(dto: BoardUserDTO): Boolean {
        val q =
            Insert(tableName())
                .into(Db.BoardsUsers.BOARD_ID, Db.BoardsUsers.USER_ID, Db.BoardsUsers.IS_USER_OWNER)
                .values(Db.Q, Db.Q, Db.Q)
                .build()
        val ps = conn.prepareStatement(q)
        ps.apply {
            setString(1, dto.boardId)
            setString(2, dto.userId)
            setBoolean(3, dto.isUserOwner)
        }
        return ps.execute()
    }


    fun findByBoardId(boardId: String): List<BoardUserDTO> {
        return findByParam(Db.BoardsUsers.BOARD_ID, boardId, Db.BoardsUsers.USER_ID)
    }

    fun findByUserId(userId: String): List<BoardUserDTO> {
        return findByParam(Db.BoardsUsers.USER_ID, userId, Db.BoardsUsers.BOARD_ID)
    }

    fun find(userId: String, boardId: String): BoardUserDTO? {
        return findFirstByTwoParam(Db.BoardsUsers.USER_ID, userId, Db.BoardsUsers.BOARD_ID, boardId)
    }

    override fun toDaoObject(rs: ResultSet): BoardUserDTO = BoardUserDTO(
        rs.getString(1),
        rs.getString(2),
        rs.getBoolean(3)
    )

    fun deleteByBoardAndUsersId(boardId: String, userId: String) {
        val q = Delete(tableName()).where(Db.BoardsUsers.BOARD_ID, CD.EQ, Db.Q).and(Db.BoardsUsers.USER_ID, CD.EQ, Db.Q).build()
        val prep = conn.prepareStatement(q)
        prep.apply {
            setString(1, boardId)
            setString(2, userId)
        }
        prep.execute()
    }
}
package com.erhn.ftknft.notesserver.data.repository

import com.erhn.ftknft.notesserver.data.dao.UserDao
import com.erhn.ftknft.notesserver.data.dto.UserDTO
import com.erhn.ftknft.notesserver.domain.repository.UserRepository

class UserRepositoryImpl(private val userDao: UserDao) : UserRepository {


    override fun createUser(userDTO: UserDTO) {
        userDao.insert(userDTO)
    }

    override fun getUserByLoginAndPassword(login: String, password: String): UserDTO? {
        return userDao.findByLoginAndPassword(login, password)
    }

    override fun getUserByLogin(login: String): UserDTO? {
        return userDao.findByLogin(login)
    }

    override fun getUserById(userId: String): UserDTO? {
        return userDao.findById(userId)
    }
}
package com.erhn.ftknft.notesserver.data

import com.erhn.ftknft.notesserver.data.dao.*
import org.sqlite.SQLiteConfig
import java.sql.Connection
import java.sql.DriverManager

class Database {

    private val conn: Connection
    val userDao: UserDao
    val refreshTokenDao: RefreshTokenDao
    val boardDao: BoardDao
    val taskDao: TaskDao
    val boardUserDao: BoardUserDao


    init {
        Class.forName("org.sqlite.JDBC")
        val config = SQLiteConfig()
        config.enforceForeignKeys(true)
        val name = "Db.db"
        conn = DriverManager.getConnection("jdbc:sqlite:$name", config.toProperties())
        userDao = UserDao(conn)
        refreshTokenDao = RefreshTokenDao(conn)
        boardDao = BoardDao(conn)
        taskDao = TaskDao(conn)
        boardUserDao = BoardUserDao(conn)
    }
}
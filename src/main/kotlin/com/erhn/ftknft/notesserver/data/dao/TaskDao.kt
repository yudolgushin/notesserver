package com.erhn.ftknft.notesserver.data.dao

import com.erhn.ftknft.notesserver.data.Db
import com.erhn.ftknft.notesserver.data.dto.TaskDTO
import com.erhn.ftknft.notesserver.presentation.model.UpdateTaskRequest
import com.erhn.ftknft.notesserver.utils.querybuilder.CreateTable
import com.erhn.ftknft.notesserver.utils.querybuilder.Insert
import com.erhn.ftknft.notesserver.utils.querybuilder.Update
import com.erhn.ftknft.notesserver.utils.querybuilder.core.CD
import com.erhn.ftknft.notesserver.utils.querybuilder.core.IF_NOT_EXISTS
import com.erhn.ftknft.notesserver.utils.querybuilder.core.columnconstraint.Type
import java.sql.Connection
import java.sql.PreparedStatement
import java.sql.ResultSet

class TaskDao(conn: Connection) : BaseDao<TaskDTO>(conn) {

    override fun tableName(): String = Db.Tasks.TABLE_NAME

    override fun createTableQuery(): String =
        CreateTable(tableName(), IF_NOT_EXISTS)
            .iniColumns()
            .column(Db.Tasks.ID, Type.TEXT).primaryKey().notNull()
            .column(Db.Tasks.BOARD_ID, Type.TEXT).notNull()
            .column(Db.Tasks.OWNER_ID, Type.TEXT).notNull()
            .column(Db.Tasks.TITLE, Type.TEXT).notNull()
            .column(Db.Tasks.IS_COMPLETE, Type.INTEGER).notNull()
            .column(Db.Tasks.CREATE_DATE, Type.TEXT).notNull()
            .foreignKey(Db.Tasks.BOARD_ID).references(Db.Boards.TABLE_NAME, Db.Boards.ID).onDeleteCascade()
            .foreignKey(Db.Tasks.OWNER_ID).references(Db.Users.TABLE_NAME, Db.Users.ID).onDeleteCascade()
            .endInits()
            .build()

    override fun insert(dto: TaskDTO): Boolean {
        val q = Insert(tableName())
            .into(Db.Tasks.ID, Db.Tasks.BOARD_ID, Db.Tasks.OWNER_ID, Db.Tasks.TITLE, Db.Tasks.IS_COMPLETE, Db.Tasks.CREATE_DATE)
            .values(Db.Q, Db.Q, Db.Q, Db.Q, Db.Q, Db.Q)
            .build()
        val ps = conn.prepareStatement(q)
        ps.apply {
            setString(1, dto.id)
            setString(2, dto.boardId)
            setString(3, dto.ownerId)
            setString(4, dto.title)
            setBoolean(5, dto.isComplete)
            setString(6, dto.createdDate)
        }
        return ps.execute()
    }

    fun insertAll(tasks: List<TaskDTO>) {
        var into = Insert(tableName())
            .into(Db.Tasks.ID, Db.Tasks.BOARD_ID, Db.Tasks.OWNER_ID, Db.Tasks.TITLE, Db.Tasks.IS_COMPLETE, Db.Tasks.CREATE_DATE)
        for (task in tasks) {
            into = into.values(Db.Q, Db.Q, Db.Q, Db.Q, Db.Q, Db.Q)
        }
        val q = into.build()
        val ps = conn.prepareStatement(q)
        ps.apply {
            val size = 6
            for (task in tasks) {
                val index = tasks.indexOf(task)
                val psIndex = if (index == 0) 1 else (index * size) + 1
                setString(psIndex, task.id)
                setString(psIndex + 1, task.boardId)
                setString(psIndex + 2, task.ownerId)
                setString(psIndex + 3, task.title)
                setBoolean(psIndex + 4, task.isComplete)
                setString(psIndex + 5, task.createdDate)
            }
        }
        ps.execute()
    }

    override fun toDaoObject(rs: ResultSet): TaskDTO {
        return TaskDTO(
            id = rs.getString(1),
            boardId = rs.getString(2),
            ownerId = rs.getString(3),
            title = rs.getString(4),
            isComplete = rs.getBoolean(5),
            createdDate = rs.getString(6)
        )
    }

    fun findTasksByBoardsIds(boardsIds: List<String>): List<TaskDTO> {
        if (boardsIds.isEmpty()) return listOf()
        var queryIds = "("
        for (i in 0 until boardsIds.size) {
            when {
                i == boardsIds.lastIndex -> queryIds += "?)"
                else -> queryIds += "?, "
            }
        }
        val query = "SELECT * FROM ${tableName()} WHERE ${Db.Tasks.BOARD_ID} IN ${queryIds};"
        println(query)
        val prep = conn.prepareStatement(query)
        for (i in 1..boardsIds.size) {
            prep.setString(i, boardsIds[i - 1])
        }
        return prep.executeQuery().toList()
    }

    fun findById(taskId: String): TaskDTO? {
        return findFirstByParam(Db.Tasks.ID, taskId)
    }

    fun deleteById(id: String) {
        deleteByParam(Db.Tasks.ID, id)
    }

    fun deleteAllByBoardId(boardId: String) {
        deleteByParam(Db.Tasks.BOARD_ID, boardId)
    }

    fun updateTask(boardId: String, task: UpdateTaskRequest) {
        if (task.title == null && task.isComplete == null) return
        val queryBuilder = Update(tableName())
        val preps: PreparedStatement = if (task.title != null && task.isComplete != null) {
            val query = queryBuilder.set(Db.Tasks.TITLE, Db.Q)
                .set(Db.Tasks.IS_COMPLETE, Db.Q)
                .where(Db.Tasks.ID, CD.EQ, Db.Q)
                .and(Db.Tasks.BOARD_ID, CD.EQ, Db.Q)
                .build()
            val prep = conn.prepareStatement(query)
            prep.apply {
                setString(1, task.title)
                setBoolean(2, task.isComplete)
                setString(3, task.id)
                setString(4, boardId)
            }

        } else if (task.title != null) {
            val query = queryBuilder.set(Db.Tasks.TITLE, Db.Q)
                .where(Db.Tasks.ID, CD.EQ, Db.Q)
                .and(Db.Tasks.BOARD_ID, CD.EQ, Db.Q)
                .build()
            val prep = conn.prepareStatement(query)
            prep.apply {
                setString(1, task.title)
                setString(2, task.id)
                setString(3, boardId)
            }
        } else if (task.isComplete != null) {
            val query = queryBuilder.set(Db.Tasks.IS_COMPLETE, Db.Q)
                .where(Db.Tasks.ID, CD.EQ, Db.Q)
                .and(Db.Tasks.BOARD_ID, CD.EQ, Db.Q)
                .build()
            val prep = conn.prepareStatement(query)
            prep.apply {
                setBoolean(1, task.isComplete)
                setString(2, task.id)
                setString(3, boardId)
            }
        } else {
            throw RuntimeException()
        }
        preps.execute()
    }
}
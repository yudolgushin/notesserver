package com.erhn.ftknft.notesserver.data.dto

data class BoardUserDTO(
    val boardId: String,
    val userId: String,
    val isUserOwner: Boolean
)
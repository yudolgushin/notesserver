package com.erhn.ftknft.notesserver.data.repository

import com.erhn.ftknft.notesserver.data.dao.RefreshTokenDao
import com.erhn.ftknft.notesserver.data.dto.RefreshTokenDTO
import com.erhn.ftknft.notesserver.domain.repository.TokenRepository

class TokenRepositoryImpl(private val refreshTokenDao: RefreshTokenDao) : TokenRepository {

    override fun createRefreshToken(refreshTokenDTO: RefreshTokenDTO) {
        refreshTokenDao.insert(refreshTokenDTO)
    }

    override fun getRefreshTokenByValue(refreshTokenValue: String): RefreshTokenDTO? {
        return refreshTokenDao.find(refreshTokenValue)
    }

    override fun deleteToken(token: RefreshTokenDTO) {
        refreshTokenDao.delete(token)
    }
}
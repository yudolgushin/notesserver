package com.erhn.ftknft.notesserver.data.repository

import com.erhn.ftknft.notesserver.data.dao.BoardDao
import com.erhn.ftknft.notesserver.data.dao.BoardUserDao
import com.erhn.ftknft.notesserver.data.dto.BoardDTO
import com.erhn.ftknft.notesserver.data.dto.BoardUserDTO
import com.erhn.ftknft.notesserver.domain.repository.BoardRepository

class BoardRepositoryImpl(private val boardDao: BoardDao, private val boardUserDao: BoardUserDao) : BoardRepository {
    override fun getBoardUserPairs(boardId: String): List<BoardUserDTO> {
        return boardUserDao.findByBoardId(boardId)
    }

    override fun createBoard(newBoard: BoardDTO) {
        boardDao.insert(newBoard)
        boardUserDao.insert(BoardUserDTO(newBoard.id, newBoard.ownerId, true))
    }

    override fun deleteById(boardId: String) {
        boardDao.deleteById(boardId)
    }

    override fun findAllByUserId(userId: String): List<BoardDTO> {
        return boardDao.findAllByUserId(userId)
    }

    override fun updateBoardTitle(boardId: String, title: String) {
        boardDao.updateBoardTitle(boardId, title)
    }

    override fun findById(boardId: String): BoardDTO? {
        return boardDao.findById(boardId)
    }

    override fun addToBoard(boardId: String, userId: String) {
        boardUserDao.insert(BoardUserDTO(boardId, userId, isUserOwner = false))
    }
}
package com.erhn.ftknft.notesserver.data

object Db {
    const val Q = "?"

    object Users {
        const val TABLE_NAME = "users"
        const val ID = "id"
        const val LOGIN = "login"
        const val PASSWORD = "password"
        const val CREATE_DATE = "create_date"
    }

    object RefreshTokens {
        const val TABLE_NAME = "refresh_tokens"
        const val USER_ID = "user_id"
        const val TOKEN = "token"
        const val EXPIRE_DATE = "expire_date"
    }

    object Boards {
        const val TABLE_NAME = "boards"
        const val ID: String = "id"
        const val TITLE = "title"
        const val OWNER_ID: String = "owner_id"
        const val CREATE_DATE = "create_date"
        const val UPDATE_DATE = "update_date"
    }

    object Tasks {
        const val TABLE_NAME = "tasks"
        const val ID = "id"
        const val BOARD_ID = "board_id"
        const val OWNER_ID = "owner_id"
        const val TITLE = "title"
        const val IS_COMPLETE = "is_complete"
        const val CREATE_DATE = "create_date"
    }

    object BoardsUsers {
        const val TABLE_NAME = "boards_users"
        const val BOARD_ID = "board_id"
        const val USER_ID = "user_id"
        const val IS_USER_OWNER = "is_user_owner"
    }

    object Invite {
        const val TABLE_NAME = "invites"
        const val ID = "id"
        const val BOARD_ID = "board_id"
        const val OWNER_ID = "owner_id"
        const val GUEST_ID = "guest_id"
        const val STATUS = "status"
        const val CREATE_DATE = "create_date"
        const val UPDATE_DATE = "update_date"
    }
}
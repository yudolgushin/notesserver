package com.erhn.ftknft.notesserver.data.dto

data class TaskDTO(
    val id: String,
    val boardId: String,
    val ownerId: String,
    val title: String,
    val isComplete: Boolean,
    val createdDate: String
)
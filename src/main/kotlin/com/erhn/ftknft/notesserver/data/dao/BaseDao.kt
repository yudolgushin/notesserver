package com.erhn.ftknft.notesserver.data.dao

import com.erhn.ftknft.notesserver.data.Db
import com.erhn.ftknft.notesserver.utils.querybuilder.Delete
import com.erhn.ftknft.notesserver.utils.querybuilder.Select
import com.erhn.ftknft.notesserver.utils.querybuilder.core.CD
import com.erhn.ftknft.notesserver.utils.querybuilder.core.DESC
import java.sql.Connection
import java.sql.ResultSet

abstract class BaseDao<T>(protected val conn: Connection) {

    init {
        createTable()
    }

    fun createTable() {
        val prep = conn.prepareStatement(createTableQuery())
        prep.execute()
    }

    abstract fun tableName(): String

    abstract fun createTableQuery(): String

    abstract fun insert(dto: T): Boolean

    abstract fun toDaoObject(rs: ResultSet): T

    fun getAll(): List<T> {
        val query = Select().from(tableName()).build()
        val ps = conn.prepareStatement(query)
        val rs = ps.executeQuery()
        val elements = ArrayList<T>()
        while (rs.next()) {
            elements.add(toDaoObject(rs))
        }
        return elements
    }

    fun findByParam(columnName: String, value: Any, columnOrder: String): List<T> {
        val query = Select().from(tableName())
            .where(columnName, CD.EQ, Db.Q)
            .orderBy(columnOrder, DESC)
            .build()
        val prepState = conn.prepareStatement(query)
        prepState.setObject(1, value)
        val rs = prepState.executeQuery()
        val elements = ArrayList<T>()
        while (rs.next()) {
            elements.add(toDaoObject(rs))
        }
        return elements
    }

    fun findFirstByParam(columnName: String, value: Any): T? {
        val query = Select().from(tableName())
            .where(columnName, CD.EQ, Db.Q)
            .limit(1)
            .build()
        val prepState = conn.prepareStatement(query)
        prepState.setObject(1, value)
        val rs = prepState.executeQuery()
        return objectFromResultSet(rs)
    }

    fun findByTwoParam(
        fisrtColumnName: String,
        firstValue: Any,
        secondColumnName: String,
        secondValue: Any,
        columnOrder: String
    ): List<T> {
        val query = Select().from(tableName())
            .where(fisrtColumnName, CD.EQ, Db.Q)
            .and(secondColumnName, CD.EQ, Db.Q)
            .orderBy(columnOrder, DESC)
            .build()
        val prepState = conn.prepareStatement(query)
        prepState.setObject(1, firstValue)
        prepState.setObject(2, secondValue)
        val rs = prepState.executeQuery()
        val elements = ArrayList<T>()
        while (rs.next()) {
            elements.add(toDaoObject(rs))
        }
        return elements
    }

    fun findFirstByTwoParam(firstColumnName: String, firstValue: Any, secondColumnName: String, secondValue: Any): T? {
        val query = Select().from(tableName())
            .where(firstColumnName, CD.EQ, Db.Q)
            .and(secondColumnName, CD.EQ, Db.Q)
            .limit(1)
            .build()
        val prepState = conn.prepareStatement(query)
        prepState.setObject(1, firstValue)
        prepState.setObject(2, secondValue)
        val rs = prepState.executeQuery()
        return objectFromResultSet(rs)
    }

    fun deleteByParam(columnName: String, value: Any) {
        val query = Delete(tableName()).where(columnName, CD.EQ, Db.Q)
            .build()
        val ps = conn.prepareStatement(query)
        ps.setObject(1, value)
        ps.execute()
    }

    protected fun ResultSet.toList(): List<T> {
        val list = ArrayList<T>()
        while (this.next()) {
            list.add(toDaoObject(this))
        }
        return list
    }


    private fun objectFromResultSet(executeQuery: ResultSet): T? {
        if (!executeQuery.isBeforeFirst) {
            return null
        } else {
            return toDaoObject(executeQuery)
        }
    }
}
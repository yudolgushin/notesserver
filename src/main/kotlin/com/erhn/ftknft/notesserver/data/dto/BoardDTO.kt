package com.erhn.ftknft.notesserver.data.dto

data class BoardDTO(
    val id: String,
    val ownerId: String,
    val title: String,
    val createDate: String,
    val updateDate: String
)
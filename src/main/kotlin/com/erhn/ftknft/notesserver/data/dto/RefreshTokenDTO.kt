package com.erhn.ftknft.notesserver.data.dto

data class RefreshTokenDTO(
    val userId: String,
    val token: String,
    val expireDate: Long
)
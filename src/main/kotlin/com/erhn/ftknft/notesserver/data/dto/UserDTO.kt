package com.erhn.ftknft.notesserver.data.dto

data class UserDTO(
    val id: String,
    val login: String,
    val password: String,
    val createDate: String
)
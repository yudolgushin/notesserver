package com.erhn.ftknft.notesserver.data.repository

import com.erhn.ftknft.notesserver.data.dao.BoardDao
import com.erhn.ftknft.notesserver.data.dao.TaskDao
import com.erhn.ftknft.notesserver.data.dto.TaskDTO
import com.erhn.ftknft.notesserver.domain.repository.TaskRepository
import com.erhn.ftknft.notesserver.presentation.model.UpdateTaskRequest

class TaskRepositoryImpl(private val taskDao: TaskDao, private val boardDao: BoardDao) : TaskRepository {
    override fun insertAll(tasksDTOs: List<TaskDTO>) {
        taskDao.insertAll(tasksDTOs)
    }

    override fun deleteAllByBoardId(boardId: String) {
        taskDao.deleteAllByBoardId(boardId)
        boardDao.updateBoard(boardId)
    }

    override fun findById(taskId: String): TaskDTO? {
        return taskDao.findById(taskId)
    }

    override fun deleteById(boardId: String, id: String) {
        taskDao.deleteById(id)
    }

    override fun findTasksByBoardsIds(map: List<String>): List<TaskDTO> {
        return taskDao.findTasksByBoardsIds(map)
    }

    override fun updateTask(boardId: String, task: UpdateTaskRequest) {
        taskDao.updateTask(boardId, task)
        boardDao.updateBoard(boardId)
    }
}
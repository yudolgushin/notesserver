package com.erhn.ftknft.notesserver.data.dao

import com.erhn.ftknft.notesserver.data.Db
import com.erhn.ftknft.notesserver.data.dto.UserDTO
import com.erhn.ftknft.notesserver.utils.querybuilder.CreateTable
import com.erhn.ftknft.notesserver.utils.querybuilder.Insert
import com.erhn.ftknft.notesserver.utils.querybuilder.core.IF_NOT_EXISTS
import com.erhn.ftknft.notesserver.utils.querybuilder.core.columnconstraint.Type
import java.sql.Connection
import java.sql.ResultSet

class UserDao(connection: Connection) : BaseDao<UserDTO>(connection) {

    override fun tableName(): String = Db.Users.TABLE_NAME

    override fun createTableQuery(): String =
        CreateTable(tableName(), IF_NOT_EXISTS).iniColumns()
            .column(Db.Users.ID, Type.TEXT).primaryKey().notNull()
            .column(Db.Users.LOGIN, Type.TEXT).notNull().unique()
            .column(Db.Users.PASSWORD, Type.TEXT).notNull()
            .column(Db.Users.CREATE_DATE, Type.TEXT).notNull()
            .endInits().build()

    override fun toDaoObject(rs: ResultSet): UserDTO {
        return UserDTO(
            rs.getString(1),
            rs.getString(2),
            rs.getString(3),
            rs.getString(4),
        )
    }

    override fun insert(dto: UserDTO): Boolean {
        val query = Insert(tableName())
            .into(Db.Users.ID, Db.Users.LOGIN, Db.Users.PASSWORD, Db.Users.CREATE_DATE)
            .values(Db.Q, Db.Q, Db.Q, Db.Q)
            .build()
        val prepState = conn.prepareStatement(query)
        prepState.apply {
            setString(1, dto.id)
            setString(2, dto.login)
            setString(3, dto.password)
            setString(4, dto.createDate)
        }
        return prepState.execute()
    }

    fun findByLogin(login: String): UserDTO? {
        return findFirstByParam(Db.Users.LOGIN, login)
    }

    fun findById(id: String): UserDTO? {
        return findFirstByParam(Db.Users.ID, id)
    }

    fun findByLoginAndPassword(login: String, password: String): UserDTO? {
        return findFirstByTwoParam(Db.Users.LOGIN, login, Db.Users.PASSWORD, password)
    }
}
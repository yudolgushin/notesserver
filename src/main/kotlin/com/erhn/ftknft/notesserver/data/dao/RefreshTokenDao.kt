package com.erhn.ftknft.notesserver.data.dao

import com.erhn.ftknft.notesserver.data.Db
import com.erhn.ftknft.notesserver.data.dto.RefreshTokenDTO
import com.erhn.ftknft.notesserver.utils.querybuilder.CreateTable
import com.erhn.ftknft.notesserver.utils.querybuilder.Insert
import com.erhn.ftknft.notesserver.utils.querybuilder.core.IF_NOT_EXISTS
import com.erhn.ftknft.notesserver.utils.querybuilder.core.columnconstraint.Type
import java.sql.Connection
import java.sql.ResultSet

class RefreshTokenDao(conn: Connection) : BaseDao<RefreshTokenDTO>(conn) {

    override fun tableName(): String = Db.RefreshTokens.TABLE_NAME

    override fun createTableQuery(): String = CreateTable(tableName(), IF_NOT_EXISTS).iniColumns()
        .column(Db.RefreshTokens.USER_ID, Type.TEXT).notNull()
        .column(Db.RefreshTokens.TOKEN, Type.TEXT).notNull()
        .column(Db.RefreshTokens.EXPIRE_DATE, Type.INTEGER).notNull()
        .foreignKey(Db.RefreshTokens.USER_ID).references(Db.Users.TABLE_NAME, Db.Users.ID).onDeleteCascade()
        .endInits()
        .build()

    override fun insert(dto: RefreshTokenDTO): Boolean {
        val query = Insert(tableName()).into(Db.RefreshTokens.USER_ID, Db.RefreshTokens.TOKEN, Db.RefreshTokens.EXPIRE_DATE)
            .values(Db.Q, Db.Q, Db.Q).build()
        val prespState = conn.prepareStatement(query)
        prespState.apply {
            setString(1, dto.userId)
            setString(2, dto.token)
            setLong(3, dto.expireDate)
        }
        return prespState.execute()
    }

    override fun toDaoObject(rs: ResultSet): RefreshTokenDTO {
        return RefreshTokenDTO(rs.getString(1), rs.getString(2), rs.getLong(3))
    }

    fun find(refreshToken: String): RefreshTokenDTO? {
        return findFirstByParam(Db.RefreshTokens.TOKEN, refreshToken)
    }

    fun delete(token: RefreshTokenDTO) {
        return deleteByParam(Db.RefreshTokens.TOKEN, token.token)
    }
}
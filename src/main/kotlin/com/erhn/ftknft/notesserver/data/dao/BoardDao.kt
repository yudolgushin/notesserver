package com.erhn.ftknft.notesserver.data.dao

import com.erhn.ftknft.notesserver.data.Db
import com.erhn.ftknft.notesserver.data.dto.BoardDTO
import com.erhn.ftknft.notesserver.utils.querybuilder.CreateTable
import com.erhn.ftknft.notesserver.utils.querybuilder.Insert
import com.erhn.ftknft.notesserver.utils.querybuilder.Update
import com.erhn.ftknft.notesserver.utils.querybuilder.core.CD
import com.erhn.ftknft.notesserver.utils.querybuilder.core.IF_NOT_EXISTS
import com.erhn.ftknft.notesserver.utils.querybuilder.core.columnconstraint.Type
import java.sql.Connection
import java.sql.ResultSet
import java.time.OffsetDateTime

class BoardDao(conn: Connection) : BaseDao<BoardDTO>(conn) {

    override fun tableName(): String = Db.Boards.TABLE_NAME

    override fun createTableQuery(): String =
        CreateTable(tableName(), IF_NOT_EXISTS)
            .iniColumns()
            .column(Db.Boards.ID, Type.TEXT).primaryKey().notNull()
            .column(Db.Boards.OWNER_ID, Type.TEXT).notNull()
            .column(Db.Boards.TITLE, Type.TEXT).notNull()
            .column(Db.Boards.CREATE_DATE, Type.TEXT).notNull()
            .column(Db.Boards.UPDATE_DATE, Type.TEXT).notNull()
            .foreignKey(Db.Boards.OWNER_ID).references(Db.Users.TABLE_NAME, Db.Users.ID).onDeleteCascade()
            .endInits()
            .build()

    override fun insert(dto: BoardDTO): Boolean {
        val query = Insert(tableName())
            .into(Db.Boards.ID, Db.Boards.OWNER_ID, Db.Boards.TITLE, Db.Boards.CREATE_DATE, Db.Boards.UPDATE_DATE)
            .values(Db.Q, Db.Q, Db.Q, Db.Q, Db.Q)
            .build()
        val ps = conn.prepareStatement(query)
        ps.apply {
            setString(1, dto.id)
            setString(2, dto.ownerId)
            setString(3, dto.title)
            setString(4, dto.createDate)
            setString(5, dto.updateDate)
        }
        return ps.execute()
    }

    override fun toDaoObject(rs: ResultSet): BoardDTO {
        return BoardDTO(
            rs.getString(1),
            rs.getString(2),
            rs.getString(3),
            rs.getString(4),
            rs.getString(5)
        )
    }

    fun findAllByUserId(userId: String): List<BoardDTO> {
        val query =
            "SELECT " +
                    "${tableName()}.${Db.Boards.ID}," +
                    " ${tableName()}.${Db.Boards.OWNER_ID}," +
                    " ${tableName()}.${Db.Boards.TITLE}," +
                    " ${tableName()}.${Db.Boards.CREATE_DATE}, " +
                    "${tableName()}.${Db.Boards.UPDATE_DATE} FROM ${tableName()} " +
                    "INNER JOIN ${Db.BoardsUsers.TABLE_NAME} ON ${Db.BoardsUsers.TABLE_NAME}.${Db.BoardsUsers.BOARD_ID} = ${tableName()}.${Db.Boards.ID} " +
                    "WHERE ${Db.BoardsUsers.TABLE_NAME}.${Db.BoardsUsers.USER_ID} = ? ORDER BY ${Db.Boards.UPDATE_DATE} DESC;"
        println(query)
        val prep = conn.prepareStatement(query)
        prep.setString(1, userId)
        val rs = prep.executeQuery()
        return rs.toList()
    }

    fun findById(boardId: String): BoardDTO? {
        return findFirstByParam(Db.Boards.ID, boardId)
    }

    fun deleteById(boardId: String) {
        deleteByParam(Db.Boards.ID, boardId)
    }

    fun updateBoardTitle(boardId: String, boardName: String) {
        val time = OffsetDateTime.now().toString()
        val query = Update(tableName()).set(Db.Boards.TITLE, "?").set(Db.Boards.UPDATE_DATE, Db.Q).where(Db.Boards.ID, CD.EQ, "?").build()
        val prep = conn.prepareStatement(query)
        prep.apply {
            setString(1, boardName)
            setString(2, time)
            setString(3, boardId)
        }
        prep.execute()
    }

    fun updateBoard(boardId: String) {
        val time = OffsetDateTime.now().toString()
        val query = Update(tableName()).set(Db.Boards.UPDATE_DATE, Db.Q).where(Db.Boards.ID, CD.EQ , Db.Q).build()
        val perp = conn.prepareStatement(query)
        perp.apply {
            setString(1, time)
            setString(2,boardId)
        }
        perp.execute()
    }
}
package com.erhn.ftknft.notesserver.domain.services.refreshtokens

import com.erhn.ftknft.notesserver.data.dto.UserDTO
import com.erhn.ftknft.notesserver.domain.exceptions.BaseException
import com.erhn.ftknft.notesserver.domain.repository.TokenRepository
import com.erhn.ftknft.notesserver.domain.repository.UserRepository

class FindRefreshTokenServiceImpl(
    private val tokenRepository: TokenRepository, private val userRepository: UserRepository
) : FindRefreshTokenService {

    override fun findUserByRefreshToken(refreshToken: String): UserDTO {
        val token = tokenRepository.getRefreshTokenByValue(refreshToken)
        if (token == null || token.expireDate < System.currentTimeMillis()) {
            throw BaseException(BaseException.Type.UNAUTHORIZED)
        }
        tokenRepository.deleteToken(token)
        val user = userRepository.getUserById(token.userId)
        if (user == null) {
            throw  BaseException(BaseException.Type.USER_NOT_EXISTS)
        }
        return user
    }
}
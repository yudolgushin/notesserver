package com.erhn.ftknft.notesserver.domain.utils

import java.util.concurrent.TimeUnit

object TokenUtils {
    const val SECRET_KEY = "?ewa{/=,yj927DTs3tT#CFR_whxmwyu."
    val ACCESS_EXPIRED = TimeUnit.DAYS.toMillis(1)
    val REFRESH_EXPIRED = TimeUnit.DAYS.toMillis(30)
}
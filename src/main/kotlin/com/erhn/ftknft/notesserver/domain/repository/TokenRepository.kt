package com.erhn.ftknft.notesserver.domain.repository

import com.erhn.ftknft.notesserver.data.dto.RefreshTokenDTO

interface TokenRepository {
    fun createRefreshToken(refreshTokenDTO: RefreshTokenDTO)
    fun getRefreshTokenByValue(refreshTokenValue: String): RefreshTokenDTO?
    fun deleteToken(token: RefreshTokenDTO)
}
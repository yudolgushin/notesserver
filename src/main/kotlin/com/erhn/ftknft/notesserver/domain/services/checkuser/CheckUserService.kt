package com.erhn.ftknft.notesserver.domain.services.checkuser

interface CheckUserService {
    fun userIsExists(userId: String): Boolean
}
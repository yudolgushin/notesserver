package com.erhn.ftknft.notesserver.domain.repository

import com.erhn.ftknft.notesserver.data.dto.UserDTO

interface UserRepository {
    fun getUserByLoginAndPassword(login: String, password: String): UserDTO?
    fun getUserByLogin(login: String): UserDTO?
    fun createUser(userDTO: UserDTO)
    fun getUserById(userId: String): UserDTO?

}
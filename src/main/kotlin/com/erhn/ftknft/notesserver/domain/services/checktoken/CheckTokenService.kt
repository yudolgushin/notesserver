package com.erhn.ftknft.notesserver.domain.services.checktoken

import com.erhn.ftknft.notesserver.utils.jwt.model.Jwt

interface CheckTokenService {

    fun checkToken(token: String): Jwt
}
package com.erhn.ftknft.notesserver.domain.services.refreshtokens

import com.erhn.ftknft.notesserver.data.dto.UserDTO

interface FindRefreshTokenService {

    fun findUserByRefreshToken(refreshToken: String): UserDTO
}
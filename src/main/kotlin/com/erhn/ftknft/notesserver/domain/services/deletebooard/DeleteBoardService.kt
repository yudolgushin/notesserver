package com.erhn.ftknft.notesserver.domain.services.deletebooard

interface DeleteBoardService {

    fun delete(uid: String, boardId: String)
}
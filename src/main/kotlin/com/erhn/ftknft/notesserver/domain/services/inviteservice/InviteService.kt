package com.erhn.ftknft.notesserver.domain.services.inviteservice

import com.erhn.ftknft.notesserver.domain.model.InviteDomain

interface InviteService {
    fun inviteUser(invite: InviteDomain)
}
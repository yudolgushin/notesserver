package com.erhn.ftknft.notesserver.domain.repository

import com.erhn.ftknft.notesserver.data.dto.BoardDTO
import com.erhn.ftknft.notesserver.data.dto.BoardUserDTO

interface BoardRepository {

    fun getBoardUserPairs(boardId: String): List<BoardUserDTO>
    fun createBoard(newBoard: BoardDTO)
    fun deleteById(boardId: String)
    fun findAllByUserId(userId: String): List<BoardDTO>
    fun updateBoardTitle(boardId: String, title: String)
    fun findById(boardId: String): BoardDTO?
    fun addToBoard(boardId: String, userId: String)
}
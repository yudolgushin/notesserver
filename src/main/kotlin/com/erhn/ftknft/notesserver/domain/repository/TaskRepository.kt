package com.erhn.ftknft.notesserver.domain.repository

import com.erhn.ftknft.notesserver.data.dto.TaskDTO
import com.erhn.ftknft.notesserver.presentation.model.UpdateTaskRequest

interface TaskRepository {
    fun insertAll(tasksDTOs: List<TaskDTO>)
    fun deleteAllByBoardId(boardId: String)
    fun findById(taskId: String): TaskDTO?
    fun deleteById(boardId: String, id: String)
    fun findTasksByBoardsIds(map: List<String>): List<TaskDTO>
    fun updateTask(boardId: String, task: UpdateTaskRequest)
}
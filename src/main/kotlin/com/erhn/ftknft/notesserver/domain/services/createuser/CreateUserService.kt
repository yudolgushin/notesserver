package com.erhn.ftknft.notesserver.domain.services.createuser

import com.erhn.ftknft.notesserver.data.dto.UserDTO

interface CreateUserService {
    fun register(login: String, password: String): UserDTO
}
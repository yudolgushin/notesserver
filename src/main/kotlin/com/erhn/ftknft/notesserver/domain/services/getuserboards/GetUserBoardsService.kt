package com.erhn.ftknft.notesserver.domain.services.getuserboards

import com.erhn.ftknft.notesserver.presentation.model.BoardResponse

interface GetUserBoardsService {

    fun getAllUserBoards(userId: String): List<BoardResponse>
}
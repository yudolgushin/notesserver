package com.erhn.ftknft.notesserver.domain.services.getboard

import com.erhn.ftknft.notesserver.domain.exceptions.BaseException
import com.erhn.ftknft.notesserver.domain.repository.BoardRepository
import com.erhn.ftknft.notesserver.domain.repository.TaskRepository
import com.erhn.ftknft.notesserver.presentation.model.BoardResponse
import com.erhn.ftknft.notesserver.presentation.toBoardResponse
import com.erhn.ftknft.notesserver.presentation.toTaskResponse

class GetBoardServiceImpl(private val taskRepository: TaskRepository, private val boardRepository: BoardRepository) : GetBoardService {

    override fun getBoardById(boardId: String): BoardResponse {
        val board = boardRepository.findById(boardId)
        if (board == null) {
            throw BaseException(BaseException.Type.NOT_FOUND)
        }
        val tasks = taskRepository.findTasksByBoardsIds(listOf(boardId))
        return board.let { b-> b.toBoardResponse(tasks.map { it.toTaskResponse() }) }
    }
}
package com.erhn.ftknft.notesserver.domain.services.inviteservice

import com.erhn.ftknft.notesserver.domain.exceptions.BaseException
import com.erhn.ftknft.notesserver.domain.model.InviteDomain
import com.erhn.ftknft.notesserver.domain.repository.BoardRepository
import com.erhn.ftknft.notesserver.domain.services.checkuserboard.CheckBoardUserService

class InviteServiceImpl(
    private val checkBoardUserService: CheckBoardUserService,
    private val boardsRepository: BoardRepository,
) : InviteService {

    override fun inviteUser(invite: InviteDomain) {
        val role = checkBoardUserService.checkUserRole(invite.boardId, invite.ownerId)
        if (role != CheckBoardUserService.Role.OWNER) {
            throw BaseException(BaseException.Type.FORBIDDEN)
        }
        val invitedUser = boardsRepository.getBoardUserPairs(invite.boardId).find { it.userId == invite.guestId }
        if (invitedUser != null) {
            throw BaseException(BaseException.Type.BAD_REQUEST)
        }
        boardsRepository.addToBoard(invite.boardId, invite.guestId)
    }
}
package com.erhn.ftknft.notesserver.domain.services.deletebordertasks

import com.erhn.ftknft.notesserver.domain.repository.TaskRepository
import com.erhn.ftknft.notesserver.domain.services.checkuserboard.CheckBoardUserService

class DeleteBoardTasksImpl(private val checkBoardUserService: CheckBoardUserService, private val taskRepository: TaskRepository) : DeleteBoardTasks {

    override fun delete(uid: String, boardId: String) {
        val role = checkBoardUserService.checkUserRole(userId = uid, boardId = boardId)
        taskRepository.deleteAllByBoardId(boardId)
    }
}
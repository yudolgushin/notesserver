package com.erhn.ftknft.notesserver.domain.services.updatetasks

import com.erhn.ftknft.notesserver.domain.repository.TaskRepository
import com.erhn.ftknft.notesserver.presentation.model.UpdateTaskRequest

class UpdateTasksServiceImpl(private val taskRepository: TaskRepository) : UpdateTasksService {

    override fun updateTasks(boardId: String, tasks: List<UpdateTaskRequest>) {
        for (task in tasks) {
            taskRepository.updateTask(boardId, task)
        }
    }
}
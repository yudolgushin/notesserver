package com.erhn.ftknft.notesserver.domain.services.checkuserboard

interface CheckBoardUserService {

    fun checkUserRole(boardId: String, userId: String): Role

    enum class Role {
        OWNER, GUEST
    }
}
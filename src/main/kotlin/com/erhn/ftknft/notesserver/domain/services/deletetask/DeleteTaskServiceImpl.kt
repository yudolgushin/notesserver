package com.erhn.ftknft.notesserver.domain.services.deletetask

import com.erhn.ftknft.notesserver.domain.exceptions.BaseException
import com.erhn.ftknft.notesserver.domain.repository.TaskRepository
import com.erhn.ftknft.notesserver.domain.services.checkuserboard.CheckBoardUserService

class DeleteTaskServiceImpl(private val checkBoardUserService: CheckBoardUserService, private val taskRepository: TaskRepository) :
    DeleteTaskService {

    override fun delete(userId: String, boardId: String, taskId: String) {
        val role = checkBoardUserService.checkUserRole(userId = userId, boardId = boardId)
        val task = taskRepository.findById(taskId)
        if (task == null) {
            throw BaseException(BaseException.Type.NOT_FOUND)
        }
        taskRepository.deleteById(boardId, task.id)
    }
}
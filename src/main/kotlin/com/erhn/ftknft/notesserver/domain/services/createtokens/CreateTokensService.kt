package com.erhn.ftknft.notesserver.domain.services.createtokens

import com.erhn.ftknft.notesserver.data.dto.UserDTO

interface CreateTokensService {

    fun createTokensForUser(userDTO: UserDTO): Pair<String, String>
}
package com.erhn.ftknft.notesserver.domain.services.deletebordertasks

interface DeleteBoardTasks {
    fun delete(uid: String, boardId: String)
}
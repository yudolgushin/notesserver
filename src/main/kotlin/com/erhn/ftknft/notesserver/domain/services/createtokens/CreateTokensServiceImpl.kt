package com.erhn.ftknft.notesserver.domain.services.createtokens

import com.erhn.ftknft.notesserver.data.dto.RefreshTokenDTO
import com.erhn.ftknft.notesserver.data.dto.UserDTO
import com.erhn.ftknft.notesserver.domain.repository.TokenRepository
import com.erhn.ftknft.notesserver.domain.utils.TokenUtils
import com.erhn.ftknft.notesserver.presentation.JwtField
import com.erhn.ftknft.notesserver.utils.generateId
import com.erhn.ftknft.notesserver.utils.jwt.JWTHelper
import com.erhn.ftknft.notesserver.utils.jwt.model.JwtBuilder

class CreateTokensServiceImpl(private val tokenRepository: TokenRepository) : CreateTokensService {

    override fun createTokensForUser(userDTO: UserDTO): Pair<String, String> {
        val accessToken = JwtBuilder()
            .addPayload(JwtField.UID, userDTO.id)
            .addPayload(JwtField.ROLE, JwtField.Role.DEFAULT.value)
            .addPayload(JwtField.SID, generateId())
            .addPayload(JwtField.ET, System.currentTimeMillis() + TokenUtils.ACCESS_EXPIRED)
            .build()
        val signedAccess = JWTHelper.sign(accessToken, TokenUtils.SECRET_KEY)
        val refresh = generateId()
        tokenRepository.createRefreshToken(RefreshTokenDTO(userDTO.id, refresh, System.currentTimeMillis() + TokenUtils.REFRESH_EXPIRED))
        return JWTHelper.encode(signedAccess) to refresh
    }
}
package com.erhn.ftknft.notesserver.domain.services.checkuserboard

import com.erhn.ftknft.notesserver.domain.exceptions.BaseException
import com.erhn.ftknft.notesserver.domain.repository.BoardRepository

class CheckBoardUserServiceImpl(private val boardRepository: BoardRepository) : CheckBoardUserService {

    override fun checkUserRole(boardId: String, userId: String): CheckBoardUserService.Role {
        val boards = boardRepository.getBoardUserPairs(boardId)
        if (boards.isEmpty()) {
            throw BaseException(BaseException.Type.BOARD_NOT_EXISTS)
        }
        val foundUser = boards.find { it.userId == userId } ?: throw BaseException(BaseException.Type.FORBIDDEN)
        return if (foundUser.isUserOwner) CheckBoardUserService.Role.OWNER else CheckBoardUserService.Role.GUEST
    }
}
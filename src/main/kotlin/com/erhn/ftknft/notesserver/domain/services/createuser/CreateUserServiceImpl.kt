package com.erhn.ftknft.notesserver.domain.services.createuser

import com.erhn.ftknft.notesserver.data.dto.UserDTO
import com.erhn.ftknft.notesserver.domain.exceptions.BaseException
import com.erhn.ftknft.notesserver.domain.repository.UserRepository
import com.erhn.ftknft.notesserver.utils.generateId
import java.time.OffsetDateTime

class CreateUserServiceImpl(private val userRepository: UserRepository) : CreateUserService {

    override fun register(login: String, password: String): UserDTO {
        if (login.length < 6) {
            throw BaseException(BaseException.Type.INVALID_LOGIN_LENGTH)
        }
        if (password.length < 6) {
            throw BaseException(BaseException.Type.INVALID_PASSWORD_LENGTH)
        }
        val user = userRepository.getUserByLogin(login)
        if (user == null) {
            val generateId = generateId()
            val createDate = OffsetDateTime.now().toString()
            val createdUser = UserDTO(generateId, login, password, createDate)
            userRepository.createUser(createdUser)
            return createdUser
        } else {
            throw BaseException(BaseException.Type.LOGIN_ALREADY_USED)
        }
    }
}
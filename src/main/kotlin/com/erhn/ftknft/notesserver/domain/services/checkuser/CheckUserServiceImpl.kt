package com.erhn.ftknft.notesserver.domain.services.checkuser

import com.erhn.ftknft.notesserver.domain.repository.UserRepository

class CheckUserServiceImpl(private val userRepository: UserRepository) : CheckUserService {

    override fun userIsExists(userId: String): Boolean {
        val user = userRepository.getUserById(userId)
        return user != null
    }
}
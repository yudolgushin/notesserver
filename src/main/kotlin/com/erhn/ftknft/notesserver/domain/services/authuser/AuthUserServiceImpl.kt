package com.erhn.ftknft.notesserver.domain.services.authuser

import com.erhn.ftknft.notesserver.data.dto.UserDTO
import com.erhn.ftknft.notesserver.domain.exceptions.BaseException
import com.erhn.ftknft.notesserver.domain.repository.UserRepository

class AuthUserServiceImpl(private val userRepository: UserRepository) : AuthUserService {

    override fun getUser(login: String, password: String): UserDTO {
        if (login.length < 6) {
            throw BaseException(BaseException.Type.INVALID_LOGIN_LENGTH)
        }

        if (password.length < 6) {
            throw BaseException(BaseException.Type.INVALID_PASSWORD_LENGTH)
        }

        val user = userRepository.getUserByLoginAndPassword(login, password)
        if (user == null) {
            throw BaseException(BaseException.Type.USER_NOT_EXISTS)
        }
        return user
    }
}
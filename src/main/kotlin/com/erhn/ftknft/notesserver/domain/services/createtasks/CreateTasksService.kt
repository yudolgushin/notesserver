package com.erhn.ftknft.notesserver.domain.services.createtasks

import com.erhn.ftknft.notesserver.data.dto.TaskDTO
import com.erhn.ftknft.notesserver.presentation.model.NewtTask

interface CreateTasksService {

    fun createTasks(boardId: String, ownerId: String, newTasks: List<NewtTask>): List<TaskDTO>
}
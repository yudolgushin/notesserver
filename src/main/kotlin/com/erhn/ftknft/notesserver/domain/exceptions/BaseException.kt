package com.erhn.ftknft.notesserver.domain.exceptions

import com.erhn.ftknft.notesserver.presentation.StatusCode
import com.erhn.ftknft.notesserver.presentation.model.error.InternalCode

class BaseException(val type: Type) : IllegalArgumentException(type.message) {


    enum class Type(val message: String? = null, val httpCode: Int, val internalCode: Int? = null) {
        UNAUTHORIZED(httpCode = StatusCode.UNAUTHORIZED),
        BAD_REQUEST(httpCode = StatusCode.BAD_REQUEST),
        NOT_FOUND(httpCode = StatusCode.NOT_FOUND),
        FORBIDDEN(httpCode = StatusCode.FORBIDDEN),
        LOGIN_ALREADY_USED("Login already used", StatusCode.BAD_REQUEST, InternalCode.LOGIN_ALREADY_USED),
        INVALID_LOGIN_LENGTH("Login must be longer than 5 symbols", StatusCode.BAD_REQUEST, InternalCode.INVALID_LOGIN_LENGTH),
        INVALID_PASSWORD_LENGTH("Password must be longer than 5 symbols", StatusCode.BAD_REQUEST, InternalCode.INVALID_PASSWORD_LENGTH),
        USER_NOT_EXISTS("User not exists", StatusCode.UNAUTHORIZED, InternalCode.USER_NOT_EXISTS),
        BOARD_NOT_EXISTS("Board not exists", StatusCode.NOT_FOUND, InternalCode.BOARDS_NOT_EXISTS)

    }
}
package com.erhn.ftknft.notesserver.domain.services.getboard

import com.erhn.ftknft.notesserver.presentation.model.BoardResponse

interface GetBoardService {
    fun getBoardById(boardId: String): BoardResponse
}
package com.erhn.ftknft.notesserver.domain.services.getuserboards

import com.erhn.ftknft.notesserver.domain.repository.BoardRepository
import com.erhn.ftknft.notesserver.domain.repository.TaskRepository
import com.erhn.ftknft.notesserver.presentation.model.BoardResponse
import com.erhn.ftknft.notesserver.presentation.toBoardResponse
import com.erhn.ftknft.notesserver.presentation.toTaskResponse

class GetUserBoardsServiceImpl(private val boardRepository: BoardRepository, private val taskRepository: TaskRepository) : GetUserBoardsService {

    override fun getAllUserBoards(userId: String): List<BoardResponse> {
        val boards = boardRepository.findAllByUserId(userId)
        val tasks = taskRepository.findTasksByBoardsIds(boards.map { it.id })
        val boardResponses = boards.map { b ->
            b.toBoardResponse(
                tasks.filter { t -> t.boardId == b.id }
                    .map { it.toTaskResponse() })
        }
        return boardResponses
    }
}
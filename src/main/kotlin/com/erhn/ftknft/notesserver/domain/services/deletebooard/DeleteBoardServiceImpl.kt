package com.erhn.ftknft.notesserver.domain.services.deletebooard

import com.erhn.ftknft.notesserver.domain.exceptions.BaseException
import com.erhn.ftknft.notesserver.domain.repository.BoardRepository
import com.erhn.ftknft.notesserver.domain.services.checkuserboard.CheckBoardUserService

class DeleteBoardServiceImpl(
    private val checkBoardUserService: CheckBoardUserService,
    private val boardRepository: BoardRepository
) : DeleteBoardService {

    override fun delete(uid: String, boardId: String) {
        val role = checkBoardUserService.checkUserRole(userId = uid, boardId = boardId)
        if (role != CheckBoardUserService.Role.OWNER) {
            throw BaseException(BaseException.Type.FORBIDDEN)
        }
        boardRepository.deleteById(boardId)
    }
}
package com.erhn.ftknft.notesserver.domain.model

data class InviteDomain(
    val boardId: String,
    val ownerId: String,
    val guestId: String
)
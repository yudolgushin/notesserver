package com.erhn.ftknft.notesserver.domain.services.createboard

import com.erhn.ftknft.notesserver.data.dto.BoardDTO
import com.erhn.ftknft.notesserver.domain.repository.BoardRepository
import com.erhn.ftknft.notesserver.utils.generateId
import java.time.OffsetDateTime

class CreateBoardServiceImpl(private val boardRepository: BoardRepository) : CreateBoardService {

    override fun createBoard(userId: String, boardTitle: String): BoardDTO {
        val currentDate = OffsetDateTime.now().toString()
        val newBoard = BoardDTO(
            id = generateId(),
            ownerId = userId,
            title = boardTitle,
            createDate = currentDate,
            updateDate = currentDate
        )
        boardRepository.createBoard(newBoard)
        return newBoard
    }
}
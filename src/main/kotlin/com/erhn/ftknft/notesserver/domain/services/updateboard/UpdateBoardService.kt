package com.erhn.ftknft.notesserver.domain.services.updateboard

import com.erhn.ftknft.notesserver.presentation.model.BoardResponse
import com.erhn.ftknft.notesserver.presentation.model.UpdateBoardRequest

interface UpdateBoardService {

    fun updateBoard(userId: String, boardId: String, board: UpdateBoardRequest): BoardResponse
}
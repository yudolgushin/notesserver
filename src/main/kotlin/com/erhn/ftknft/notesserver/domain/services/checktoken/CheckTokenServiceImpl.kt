package com.erhn.ftknft.notesserver.domain.services.checktoken

import com.erhn.ftknft.notesserver.domain.exceptions.BaseException
import com.erhn.ftknft.notesserver.domain.repository.UserRepository
import com.erhn.ftknft.notesserver.domain.utils.TokenUtils
import com.erhn.ftknft.notesserver.utils.jwt.JWTHelper
import com.erhn.ftknft.notesserver.utils.jwt.getExpirationTime
import com.erhn.ftknft.notesserver.utils.jwt.getUID
import com.erhn.ftknft.notesserver.utils.jwt.model.Jwt

class CheckTokenServiceImpl(private val userRepository: UserRepository) : CheckTokenService {

    override fun checkToken(token: String): Jwt {
        val jwt = try {
            JWTHelper.decode(token)
        } catch (e: Throwable) {
            throw BaseException(BaseException.Type.UNAUTHORIZED)
        }
        if (JWTHelper.isValidSignature(jwt, TokenUtils.SECRET_KEY)) {
            throw BaseException(BaseException.Type.UNAUTHORIZED)
        }
        if (jwt.getExpirationTime() < System.currentTimeMillis()) {
            throw BaseException(BaseException.Type.UNAUTHORIZED)
        }
        if (userRepository.getUserById(jwt.getUID()) == null) {
            throw BaseException(BaseException.Type.USER_NOT_EXISTS)
        }
        return jwt
    }
}
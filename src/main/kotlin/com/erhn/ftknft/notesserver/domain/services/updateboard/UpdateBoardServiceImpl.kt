package com.erhn.ftknft.notesserver.domain.services.updateboard

import com.erhn.ftknft.notesserver.domain.repository.BoardRepository
import com.erhn.ftknft.notesserver.domain.services.checkuserboard.CheckBoardUserService
import com.erhn.ftknft.notesserver.domain.services.getboard.GetBoardService
import com.erhn.ftknft.notesserver.domain.services.updatetasks.UpdateTasksService
import com.erhn.ftknft.notesserver.presentation.model.BoardResponse
import com.erhn.ftknft.notesserver.presentation.model.UpdateBoardRequest

class UpdateBoardServiceImpl(
    private val checkBoardUserService: CheckBoardUserService,
    private val updateTasksService: UpdateTasksService,
    private val getBoardService: GetBoardService,
    private val boardRepository: BoardRepository
) : UpdateBoardService {

    override fun updateBoard(userId: String, boardId: String, board: UpdateBoardRequest): BoardResponse {
        val role = checkBoardUserService.checkUserRole(userId = userId, boardId = boardId)
        if (board.title != null) {
            boardRepository.updateBoardTitle(boardId, board.title)
        }
        if (board.tasks != null && board.tasks.isNotEmpty()) {
            updateTasksService.updateTasks(boardId, board.tasks)
        }
        return getBoardService.getBoardById(boardId)
    }
}
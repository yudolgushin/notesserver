package com.erhn.ftknft.notesserver.domain.services.authuser

import com.erhn.ftknft.notesserver.data.dto.UserDTO

interface AuthUserService {

    fun getUser(login: String, password: String): UserDTO
}
package com.erhn.ftknft.notesserver.domain.services.createtasks

import com.erhn.ftknft.notesserver.data.dto.TaskDTO
import com.erhn.ftknft.notesserver.domain.repository.TaskRepository
import com.erhn.ftknft.notesserver.presentation.model.NewtTask
import com.erhn.ftknft.notesserver.utils.generateId
import java.time.OffsetDateTime

class CreateTasksServiceImpl(private val taskRepository: TaskRepository) : CreateTasksService {

    override fun createTasks(boardId: String, ownerId: String, newTasks: List<NewtTask>): List<TaskDTO> {
        val tasksDTOs = newTasks.map {
            TaskDTO(
                id = generateId(),
                boardId = boardId,
                ownerId = ownerId,
                title = it.title,
                isComplete = it.isComplete ?: false,
                createdDate = OffsetDateTime.now().toString()
            )
        }
        taskRepository.insertAll(tasksDTOs)
        return tasksDTOs
    }
}
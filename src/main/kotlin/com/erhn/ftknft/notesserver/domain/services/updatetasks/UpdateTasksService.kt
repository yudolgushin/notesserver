package com.erhn.ftknft.notesserver.domain.services.updatetasks

import com.erhn.ftknft.notesserver.presentation.model.UpdateTaskRequest

interface UpdateTasksService {

    fun updateTasks(boardId: String, tasks: List<UpdateTaskRequest>)
}
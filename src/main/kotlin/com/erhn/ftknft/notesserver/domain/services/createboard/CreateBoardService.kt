package com.erhn.ftknft.notesserver.domain.services.createboard

import com.erhn.ftknft.notesserver.data.dto.BoardDTO

interface CreateBoardService {

    fun createBoard(userId: String, boardTitle: String): BoardDTO
}
package com.erhn.ftknft.notesserver.domain.services.deletetask

interface DeleteTaskService {
    fun delete(userId: String, boardId: String, taskId: String)
}
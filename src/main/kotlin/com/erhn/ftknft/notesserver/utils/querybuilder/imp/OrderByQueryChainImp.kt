package com.erhn.ftkfnt.notesserver.utils.querybuilder.imp

import com.erhn.ftknft.notesserver.utils.querybuilder.imp.BaseQueryChain

class OrderByQueryChainImp(query: String) : BaseQueryChain(query), com.erhn.ftknft.notesserver.utils.querybuilder.core.OrderByQueryChain {

    override fun orderBy(columnName: String, desc: com.erhn.ftknft.notesserver.utils.querybuilder.core.DESC?): com.erhn.ftknft.notesserver.utils.querybuilder.core.LimitQueryChain {
        query += " $ORDER_BY $columnName${desc?.let { " " + DESC } ?: ""}"
        return LimitQueryChainImp(query, OrderByQueryChainImp(query))
    }
}
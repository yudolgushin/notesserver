package com.erhn.ftknft.notesserver.utils.querybuilder.core

enum class CD(val sign: String) {
    LT("<"), GT(">"), GTEQ(">="), LTEQ("<="), EQ("="), LIKE("LIKE"), IN("IN");

    override fun toString(): String {
        return sign
    }
}
package com.erhn.ftkfnt.notesserver.utils.querybuilder.imp.columnconstraint

import com.erhn.ftknft.notesserver.utils.querybuilder.core.columnconstraint.AllConstraintQueryChain
import com.erhn.ftknft.notesserver.utils.querybuilder.core.columnconstraint.ColumnQueryChain
import com.erhn.ftknft.notesserver.utils.querybuilder.core.columnconstraint.Type
import com.erhn.ftknft.notesserver.utils.querybuilder.imp.columnconstraint.AllConstraintQueryChainImp

class ColumnQueryChainImp(query: String, private val isFirst: Boolean = false) : BaseEndInitsQueryChain(query),
    ColumnQueryChain {

    override fun column(columnName: String, type: Type): AllConstraintQueryChain {
        if (!isFirst) {
            query += ", $columnName $type"
        } else {
            query += "$columnName $type"
        }
        return AllConstraintQueryChainImp(query)
    }
}
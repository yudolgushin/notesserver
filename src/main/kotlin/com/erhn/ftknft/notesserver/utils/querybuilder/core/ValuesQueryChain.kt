package com.erhn.ftknft.notesserver.utils.querybuilder.core

interface ValuesQueryChain : QueryChain {
    fun values(vararg values: String): ValuesQueryChain

}

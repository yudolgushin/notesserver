package com.erhn.ftknft.notesserver.utils.querybuilder.core

import com.erhn.ftknft.notesserver.utils.querybuilder.imp.BaseQueryChain

interface LimitQueryChain : QueryChain,
    OrderByQueryChain {

    fun limit(limit: Int): BaseQueryChain
}
package com.erhn.ftknft.notesserver.utils

object Logger {


    fun log(tag: String, msg: String) {
        println("$tag: $msg")
    }

    fun d(msg: String) {
        val className = Thread.currentThread().stackTrace[3].className
        val methodName = Thread.currentThread().stackTrace[3].methodName
        log("$className:$methodName", msg)
    }

    fun add(obj: Any) {
        val className = obj.javaClass.name
        val methodName = Thread.currentThread().stackTrace[3].methodName
        log(className, methodName)
    }

    fun print(msg: String) {
        println(msg)
    }
}
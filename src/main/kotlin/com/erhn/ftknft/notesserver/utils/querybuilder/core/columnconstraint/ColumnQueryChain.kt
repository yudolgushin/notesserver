package com.erhn.ftknft.notesserver.utils.querybuilder.core.columnconstraint


import com.erhn.ftknft.notesserver.utils.querybuilder.core.QueryChain

interface ColumnQueryChain : QueryChain,
    EndInitsColumnsQueryChain {

    fun column(columnName: String, type: Type): AllConstraintQueryChain
}
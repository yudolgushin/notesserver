package com.erhn.ftkfnt.notesserver.utils.querybuilder.imp

import com.erhn.ftknft.notesserver.utils.querybuilder.imp.BaseQueryChain

class IntoQueryChainImp(query: String) : BaseQueryChain(query), com.erhn.ftknft.notesserver.utils.querybuilder.core.IntoQueryChain {

    override fun into(vararg columnNames: String): com.erhn.ftknft.notesserver.utils.querybuilder.core.ValuesQueryChain {
        if (columnNames.size == 0) return ValuesQueryChainImp(query)
        columnNames.forEachIndexed { index, s ->
            if (index == 0) {
                query += " ($s"
            } else {
                query += ", $s"
            }

        }
        query += ")"
        return ValuesQueryChainImp(query)
    }
}
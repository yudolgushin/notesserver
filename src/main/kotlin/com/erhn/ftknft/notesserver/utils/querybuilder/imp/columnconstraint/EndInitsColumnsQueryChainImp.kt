package com.erhn.ftkfnt.notesserver.utils.querybuilder.imp.columnconstraint

import com.erhn.ftknft.notesserver.utils.querybuilder.imp.BaseQueryChain
import com.erhn.ftknft.notesserver.utils.querybuilder.core.columnconstraint.EndInitsColumnsQueryChain

class EndInitsColumnsQueryChainImp(query: String) : BaseEndInitsQueryChain(query),
    EndInitsColumnsQueryChain {

    override fun endInits(): BaseQueryChain {
        query += ")"
        return this
    }
}
package com.erhn.ftknft.notesserver.utils.querybuilder.core.columnconstraint

import com.erhn.ftknft.notesserver.utils.querybuilder.core.QueryChain
import com.erhn.ftknft.notesserver.utils.querybuilder.core.columnconstraint.ForeignKeyQueryChain

interface OnDeleteCascadeQueryChain : QueryChain, ForeignKeyQueryChain {

    fun onDeleteCascade(): ForeignKeyQueryChain
}
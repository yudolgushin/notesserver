package com.erhn.ftknft.notesserver.utils

import java.util.*

fun generateId(): String = UUID.randomUUID().toString()
package com.erhn.ftkfnt.notesserver.utils.querybuilder.imp.columnconstraint

import com.erhn.ftknft.notesserver.utils.querybuilder.core.ReferenceQueryChain
import com.erhn.ftknft.notesserver.utils.querybuilder.core.columnconstraint.ForeignKeyQueryChain
import com.erhn.ftknft.notesserver.utils.querybuilder.core.columnconstraint.NotNullConstrainQueryChain
import com.erhn.ftknft.notesserver.utils.querybuilder.core.columnconstraint.UniqueAndNewConstraintQueryChain

class NotNullConstraintQueryChainImp(
    query: String,
    private val foreignKeyQueryChain: ForeignKeyQueryChain = ForeignKeyQueryChainImp(
        query
    )
) : BaseEndInitsQueryChain(query), NotNullConstrainQueryChain {

    override fun notNull(): UniqueAndNewConstraintQueryChain {
        query += " $NOT_NULL"
        return UniqueAndNewConstraintQueryChainImp(query)

    }

    override fun foreignKey(columnName: String): com.erhn.ftknft.notesserver.utils.querybuilder.core.ReferenceQueryChain {
        return foreignKeyQueryChain.foreignKey(columnName)
    }
}
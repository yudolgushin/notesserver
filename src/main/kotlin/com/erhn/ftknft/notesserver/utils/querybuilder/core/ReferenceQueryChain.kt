package com.erhn.ftknft.notesserver.utils.querybuilder.core

import com.erhn.ftknft.notesserver.utils.querybuilder.core.columnconstraint.OnDeleteCascadeQueryChain

interface ReferenceQueryChain : QueryChain {

    fun references(tableReference: String, columnReference: String): OnDeleteCascadeQueryChain

}

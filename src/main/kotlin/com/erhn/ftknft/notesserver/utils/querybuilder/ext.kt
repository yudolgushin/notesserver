package com.erhn.ftknft.notesserver.utils.querybuilder

import com.erhn.ftknft.notesserver.utils.querybuilder.core.IF_NOT_EXISTS


fun CreateTable(tableName: String, ifNotExists: IF_NOT_EXISTS? = null) = Qb.init().createTable(tableName, ifNotExists)

fun Insert(tableName: String) = Qb.init().insert(tableName)

fun Select(vararg columns: String) = Qb.init().select(*columns)

fun Update(tableName: String) = Qb.init().update(tableName)

fun Delete(tableName: String) = Qb.init().delete(tableName)
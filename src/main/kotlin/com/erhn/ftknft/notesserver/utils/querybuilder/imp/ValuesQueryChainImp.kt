package com.erhn.ftkfnt.notesserver.utils.querybuilder.imp

import com.erhn.ftknft.notesserver.utils.querybuilder.imp.BaseQueryChain

class ValuesQueryChainImp(query: String) : BaseQueryChain(query), com.erhn.ftknft.notesserver.utils.querybuilder.core.ValuesQueryChain {

    override fun values(vararg values: String): com.erhn.ftknft.notesserver.utils.querybuilder.core.ValuesQueryChain {
        if (values.size == 0) return this
        if (!query.contains(VALUES)) {
            query += " $VALUES"
        } else {
            query += ","
        }
        query += " ("
        values.forEachIndexed { index, s ->
            if (index == 0) {
                query += s
            } else {
                query += ", $s"
            }
        }
        query += ")"

        return ValuesQueryChainImp(query)
    }
}
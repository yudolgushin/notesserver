package com.erhn.ftkfnt.notesserver.utils.querybuilder.imp.columnconstraint

import com.erhn.ftknft.notesserver.utils.querybuilder.core.columnconstraint.ColumnQueryChain
import com.erhn.ftknft.notesserver.utils.querybuilder.core.columnconstraint.InitColumnsQueryChain

class InitColumnsQueryChainImp(query: String) : BaseEndInitsQueryChain(query),
    InitColumnsQueryChain {
    override fun iniColumns(): ColumnQueryChain {
        query += " ("
        return ColumnQueryChainImp(query, true)
    }
}
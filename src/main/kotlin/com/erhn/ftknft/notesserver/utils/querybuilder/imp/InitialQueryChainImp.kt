package com.erhn.ftkfnt.notesserver.utils.querybuilder.imp

import com.erhn.ftkfnt.notesserver.utils.querybuilder.imp.columnconstraint.InitColumnsQueryChainImp
import com.erhn.ftknft.notesserver.utils.querybuilder.core.columnconstraint.InitColumnsQueryChain
import com.erhn.ftknft.notesserver.utils.querybuilder.imp.BaseQueryChain


class InitialQueryChainImp(query: String) : BaseQueryChain(query),
    com.erhn.ftknft.notesserver.utils.querybuilder.core.InitialQueryChain {
    override fun select(vararg columns: String): com.erhn.ftknft.notesserver.utils.querybuilder.core.FromQueryChain {
        query += SELECT
        if (columns.size == 0) {
            query += " $STAR"
        } else {
            columns.forEachIndexed { index, s ->
                if (index == 0) {
                    query += " $s"
                } else {
                    query += ", $s"
                }
            }
        }
        return FromQueryChainImp(query)
    }

    override fun insert(tableName: String): com.erhn.ftknft.notesserver.utils.querybuilder.core.IntoQueryChain {
        query += "$INSERT $tableName"
        return IntoQueryChainImp(query)
    }

    override fun delete(tableName: String): com.erhn.ftknft.notesserver.utils.querybuilder.core.WhereQueryChain {
        query += "$DELETE $tableName"
        return WhereQueryChainImp(query)
    }

    override fun update(tableName: String): com.erhn.ftknft.notesserver.utils.querybuilder.core.SetQueryChain {
        query += "$UPDATE $tableName"
        return SetQueryChainImp(query)
    }

    override fun createTable(tableName: String, ifNotExists: com.erhn.ftknft.notesserver.utils.querybuilder.core.IF_NOT_EXISTS?): InitColumnsQueryChain {
        query += CREATE_TABLE
        ifNotExists?.let {
            query += " $IF_NOT_EXISTS"
        }
        query += " $tableName"

        return InitColumnsQueryChainImp(query)
    }
}
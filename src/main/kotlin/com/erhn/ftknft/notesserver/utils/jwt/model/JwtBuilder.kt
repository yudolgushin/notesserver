package com.erhn.ftknft.notesserver.utils.jwt.model

class JwtBuilder {


    private val headers = HashMap<String, String>()

    private val payloads = HashMap<String, Any>()

    private var signature: String? = null

    init {
        with(headers) {
            put(ALG_KEY, ALG)
            put(TYPE_KEY, TYPE)
        }
    }

    fun from(jwt: Jwt): JwtBuilder {
        headers.putAll(jwt.headers)
        payloads.putAll(jwt.payloads)
        signature = jwt.signature
        return this
    }

    fun addPayload(key: String, value: Any): JwtBuilder {
        payloads.put(key, value)
        return this
    }

    fun build(): Jwt {
        return Jwt(headers, payloads, signature)
    }

    companion object {
        const val ALG_KEY = "alg"
        const val ALG = "HS256"
        const val TYPE_KEY = "type"
        const val TYPE = "JWT"
    }
}
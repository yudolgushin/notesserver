package com.erhn.ftkfnt.notesserver.utils.querybuilder.imp

import com.erhn.ftknft.notesserver.utils.querybuilder.imp.AndOrQueryChainImp
import com.erhn.ftknft.notesserver.utils.querybuilder.imp.BaseQueryChain

class WhereQueryChainImp(
    query: String, val orderBy: com.erhn.ftknft.notesserver.utils.querybuilder.core.OrderByQueryChain = OrderByQueryChainImp(query),
    val limitQueryChain: com.erhn.ftknft.notesserver.utils.querybuilder.core.LimitQueryChain = LimitQueryChainImp(query, orderBy)
) :
    BaseQueryChain(query), com.erhn.ftknft.notesserver.utils.querybuilder.core.WhereQueryChain {


    override fun where(columnName: String, condition: com.erhn.ftknft.notesserver.utils.querybuilder.core.CD, value: String): com.erhn.ftknft.notesserver.utils.querybuilder.core.AndOrQueryChain {
        query += " $WHERE $columnName ${condition} $value"
        return AndOrQueryChainImp(query)
    }

    override fun wherePrep(columnName: String, condition: com.erhn.ftknft.notesserver.utils.querybuilder.core.CD): com.erhn.ftknft.notesserver.utils.querybuilder.core.AndOrQueryChain {
        return where(columnName, condition, "?")
    }

    override fun orderBy(columnName: String, desc: com.erhn.ftknft.notesserver.utils.querybuilder.core.DESC?): com.erhn.ftknft.notesserver.utils.querybuilder.core.LimitQueryChain {
        return orderBy.orderBy(columnName, desc)
    }

    override fun limit(limit: Int): BaseQueryChain {
        return limitQueryChain.limit(limit)
    }
}
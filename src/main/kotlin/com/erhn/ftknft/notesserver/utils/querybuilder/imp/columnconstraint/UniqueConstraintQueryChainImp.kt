package com.erhn.ftkfnt.notesserver.utils.querybuilder.imp.columnconstraint

import com.erhn.ftknft.notesserver.utils.querybuilder.core.ReferenceQueryChain
import com.erhn.ftknft.notesserver.utils.querybuilder.core.columnconstraint.ColumnQueryChain
import com.erhn.ftknft.notesserver.utils.querybuilder.core.columnconstraint.ForeignKeyQueryChain
import com.erhn.ftknft.notesserver.utils.querybuilder.core.columnconstraint.UniqueConstraintQueryChain

class UniqueConstraintQueryChainImp(
    query: String,
    private val foreignKeyQueryChain: ForeignKeyQueryChain = ForeignKeyQueryChainImp(
        query
    )
) : BaseEndInitsQueryChain(query), UniqueConstraintQueryChain {

    override fun unique(): ColumnQueryChain {
        query += " $UNIQUE"
        return ColumnQueryChainImp(query)
    }

    override fun foreignKey(columnName: String): com.erhn.ftknft.notesserver.utils.querybuilder.core.ReferenceQueryChain {
        return foreignKeyQueryChain.foreignKey(columnName)
    }
}
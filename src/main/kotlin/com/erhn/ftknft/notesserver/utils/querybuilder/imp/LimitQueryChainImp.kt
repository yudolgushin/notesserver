package com.erhn.ftkfnt.notesserver.utils.querybuilder.imp

import com.erhn.ftknft.notesserver.utils.querybuilder.imp.BaseQueryChain

class LimitQueryChainImp(query: String, val orderByQueryChain: com.erhn.ftknft.notesserver.utils.querybuilder.core.OrderByQueryChain) : BaseQueryChain(query),
    com.erhn.ftknft.notesserver.utils.querybuilder.core.LimitQueryChain {

    override fun limit(limit: Int): BaseQueryChain {
        query += " $LIMIT $limit"
        return LimitQueryChainImp(query, orderByQueryChain)
    }

    override fun orderBy(columnName: String, desc: com.erhn.ftknft.notesserver.utils.querybuilder.core.DESC?): com.erhn.ftknft.notesserver.utils.querybuilder.core.LimitQueryChain {
        return orderByQueryChain.orderBy(columnName, desc)
    }
}
package com.erhn.ftknft.notesserver.utils.querybuilder.core.columnconstraint

import com.erhn.ftknft.notesserver.utils.querybuilder.core.QueryChain
import com.erhn.ftknft.notesserver.utils.querybuilder.core.columnconstraint.EndInitsColumnsQueryChain

interface InitColumnsQueryChain : QueryChain,
    EndInitsColumnsQueryChain {
    fun iniColumns(): ColumnQueryChain
}
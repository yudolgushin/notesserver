package com.erhn.ftknft.notesserver.utils.jwt

import com.erhn.ftknft.notesserver.presentation.JwtField
import com.erhn.ftknft.notesserver.utils.jwt.model.Jwt
import java.util.*

fun String.toBase64(): String = Base64.getEncoder().encodeToString(this.toByteArray())

fun String.fromBase64(): String = String(Base64.getDecoder().decode(this))

fun Jwt.getUID(): String = payloads[JwtField.UID] as String
fun Jwt.getExpirationTime(): Long = (payloads[JwtField.ET] as Number).toLong()
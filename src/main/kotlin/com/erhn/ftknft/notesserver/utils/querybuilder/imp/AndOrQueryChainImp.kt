package com.erhn.ftknft.notesserver.utils.querybuilder.imp

import com.erhn.ftkfnt.notesserver.utils.querybuilder.imp.LimitQueryChainImp
import com.erhn.ftkfnt.notesserver.utils.querybuilder.imp.OrderByQueryChainImp


class AndOrQueryChainImp(
    query: String, val orderBy: com.erhn.ftknft.notesserver.utils.querybuilder.core.OrderByQueryChain = OrderByQueryChainImp(query),
    val limitQueryChain: com.erhn.ftknft.notesserver.utils.querybuilder.core.LimitQueryChain = LimitQueryChainImp(query, orderBy)
) :
    BaseQueryChain(query), com.erhn.ftknft.notesserver.utils.querybuilder.core.AndOrQueryChain {

    override fun and(columnName: String, condition: com.erhn.ftknft.notesserver.utils.querybuilder.core.CD, value: String): com.erhn.ftknft.notesserver.utils.querybuilder.core.LimitQueryChain {
        query += " $AND $columnName ${condition} $value"
        return AndOrQueryChainImp(query)
    }

    override fun or(columnName: String, condition: com.erhn.ftknft.notesserver.utils.querybuilder.core.CD, value: String): com.erhn.ftknft.notesserver.utils.querybuilder.core.LimitQueryChain {
        query += " $OR $columnName ${condition} $value"
        return AndOrQueryChainImp(query)
    }

    override fun orderBy(columnName: String, desc: com.erhn.ftknft.notesserver.utils.querybuilder.core.DESC?): com.erhn.ftknft.notesserver.utils.querybuilder.core.LimitQueryChain {
        return orderBy.orderBy(columnName, desc)
    }

    override fun limit(limit: Int): BaseQueryChain {
        return limitQueryChain.limit(limit)
    }
}

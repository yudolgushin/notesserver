package com.erhn.ftknft.notesserver.utils.querybuilder.core

interface SetAndWhereQueryChain : QueryChain,
    SetQueryChain, WhereQueryChain {
}
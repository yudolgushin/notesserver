package com.erhn.ftknft.notesserver.utils.querybuilder.core.columnconstraint

import com.erhn.ftknft.notesserver.utils.querybuilder.core.QueryChain

interface NotNullConstrainQueryChain : QueryChain, EndInitsColumnsQueryChain, ForeignKeyQueryChain {

    fun notNull(): UniqueAndNewConstraintQueryChain

}
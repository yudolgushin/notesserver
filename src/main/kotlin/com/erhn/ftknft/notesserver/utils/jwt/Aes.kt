package com.erhn.ftknft.notesserver.utils.jwt

import java.lang.IllegalArgumentException
import java.util.*
import javax.crypto.*
import javax.crypto.spec.SecretKeySpec

object Aes {

    fun encrypt(strToEncrypt: String, secret_key: String): String {
        try {
            val keyBytes = secret_key.toByteArray(charset("UTF8"))
            val skey = SecretKeySpec(keyBytes, "AES")
            val input = strToEncrypt.toByteArray(charset("UTF8"))

            synchronized(Cipher::class.java) {
                val cipher = Cipher.getInstance("AES/ECB/PKCS5Padding")
                cipher.init(Cipher.ENCRYPT_MODE, skey)

                val cipherText = ByteArray(cipher.getOutputSize(input.size))
                var ctLength = cipher.update(
                    input, 0, input.size,
                    cipherText, 0
                )
                ctLength += cipher.doFinal(cipherText, ctLength)
                return String(Base64.getEncoder().encode(cipherText))
            }
        } catch (e: Throwable) {
            throw IllegalArgumentException("Can not encrypt ${strToEncrypt}, ${e.localizedMessage}")
        }
    }

    fun decrypt(key: String, strToDecrypt: String): String {
        try {
            val keyBytes = key.toByteArray(charset("UTF8"))
            val skey = SecretKeySpec(keyBytes, "AES")
            val input = Base64.getDecoder()
                .decode(strToDecrypt.trim { it <= ' ' }.toByteArray(charset("UTF8")))
            synchronized(Cipher::class.java) {
                val cipher = Cipher.getInstance("AES/ECB/PKCS5Padding")
                cipher.init(Cipher.DECRYPT_MODE, skey)
                val plainText = ByteArray(cipher.getOutputSize(input.size))
                var ptLength = cipher.update(input, 0, input.size, plainText, 0)
                ptLength += cipher.doFinal(plainText, ptLength)
                val decryptedString = String(plainText)
                return decryptedString.trim { it <= ' ' }
            }
        } catch (e: Throwable) {
            throw IllegalArgumentException("Can not encrypt ${strToDecrypt}, ${e.localizedMessage}")
        }
    }


}
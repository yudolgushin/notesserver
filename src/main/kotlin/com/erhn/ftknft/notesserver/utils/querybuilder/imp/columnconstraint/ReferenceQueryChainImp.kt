package com.erhn.ftkfnt.notesserver.utils.querybuilder.imp.columnconstraint

import com.erhn.ftknft.notesserver.utils.querybuilder.core.ReferenceQueryChain
import com.erhn.ftknft.notesserver.utils.querybuilder.core.columnconstraint.ForeignKeyQueryChain

class ReferenceQueryChainImp(query: String, val foreignKeyQueryChain: ForeignKeyQueryChain) :
    BaseEndInitsQueryChain(query), com.erhn.ftknft.notesserver.utils.querybuilder.core.ReferenceQueryChain {
    override fun references(tableReference: String, columnReference: String): OnDeleteCascadeQueryChainImp {
        query += " $REFERENCES $tableReference($columnReference)"
        return OnDeleteCascadeQueryChainImp(query, ForeignKeyQueryChainImp(query))
    }
}
package com.erhn.ftknft.notesserver.utils.querybuilder.core

interface IntoQueryChain : QueryChain {
    fun into(vararg columnNames: String): ValuesQueryChain
}

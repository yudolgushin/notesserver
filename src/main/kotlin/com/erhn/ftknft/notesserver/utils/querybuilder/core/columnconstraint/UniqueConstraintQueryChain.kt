package com.erhn.ftknft.notesserver.utils.querybuilder.core.columnconstraint


interface UniqueConstraintQueryChain : EndInitsColumnsQueryChain, ForeignKeyQueryChain {
    fun unique(): ColumnQueryChain
}
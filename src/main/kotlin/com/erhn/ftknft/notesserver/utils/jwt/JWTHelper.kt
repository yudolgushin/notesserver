package com.erhn.ftknft.notesserver.utils.jwt

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.erhn.ftknft.notesserver.utils.jwt.model.Jwt
import java.lang.IllegalArgumentException

object JWTHelper {

    private val headersTypeToken = object : TypeToken<HashMap<String, String>>() {}.type

    private val payloadsTypeToken = object : TypeToken<HashMap<String, Any>>() {}.type

    private val gson = Gson()

    fun decode(jwtBase64: String): Jwt {
        val splits = jwtBase64.split('.')
        try {
            val headers: Map<String, String> = gson.fromJson(splits[0].fromBase64(), headersTypeToken)
            val payloads: Map<String, Any> = gson.fromJson(splits[1].fromBase64(), payloadsTypeToken)
            return if (splits.size > 2) {
                Jwt(headers, payloads, splits[2])
            } else {
                Jwt(headers, payloads, null)
            }
        } catch (t: Throwable) {
            throw IllegalArgumentException("JWT format is not correct")
        }
    }

    fun encode(jwt: Jwt): String {
        val headers = gson.toJson(jwt.headers).toBase64()
        val payloads = gson.toJson(jwt.payloads).toBase64()
        return "$headers.$payloads${jwt.signature?.let { ".$it" } ?: ""}"
    }


    fun sign(jwt: Jwt, secretKey: String): Jwt {
        if (jwt.isSigned()) {
            throw IllegalArgumentException("Jwt already signed")
        }
        val headersBase64 = gson.toJson(jwt.headers).toBase64()
        val payloadsBase64 = gson.toJson(jwt.payloads).toBase64()
        val signature = Aes.encrypt("${headersBase64}.${payloadsBase64}", secretKey)
        return Jwt(jwt.headers, jwt.payloads, signature)
    }

    fun isValidSignature(jwt: Jwt, secretKey: String): Boolean {
        if (!jwt.isSigned()) return false
        val jwtBase64 = encode(jwt)
        val splits = jwtBase64.split('.')
        val decodedSignature = "${splits[0]}.${splits[1]}"
        return Aes.decrypt(secretKey, jwt.signature!!) == decodedSignature
    }
}
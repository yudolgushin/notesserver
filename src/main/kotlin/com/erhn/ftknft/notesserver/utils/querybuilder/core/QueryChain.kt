package com.erhn.ftknft.notesserver.utils.querybuilder.core

interface QueryChain {

    fun build(): String
}
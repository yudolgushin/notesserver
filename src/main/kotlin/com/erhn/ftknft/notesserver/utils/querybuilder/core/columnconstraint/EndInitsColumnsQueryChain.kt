package com.erhn.ftknft.notesserver.utils.querybuilder.core.columnconstraint

import com.erhn.ftknft.notesserver.utils.querybuilder.core.QueryChain
import com.erhn.ftknft.notesserver.utils.querybuilder.imp.BaseQueryChain

interface EndInitsColumnsQueryChain : QueryChain {

    fun endInits(): BaseQueryChain
}
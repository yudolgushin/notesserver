package com.erhn.ftknft.notesserver.utils.querybuilder.core.columnconstraint

import com.erhn.ftknft.notesserver.utils.querybuilder.core.QueryChain

interface DefaultConstraintQueryChain : QueryChain,
    EndInitsColumnsQueryChain,
    ForeignKeyQueryChain {

    fun default(value: String): ColumnQueryChain
}
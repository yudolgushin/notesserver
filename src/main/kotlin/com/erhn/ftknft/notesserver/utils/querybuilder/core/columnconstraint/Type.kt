package com.erhn.ftknft.notesserver.utils.querybuilder.core.columnconstraint

enum class Type(val type: String) {
    INTEGER("INTEGER"), REAL("REAL"), TEXT("TEXT");

    override fun toString(): String {
        return type
    }
}
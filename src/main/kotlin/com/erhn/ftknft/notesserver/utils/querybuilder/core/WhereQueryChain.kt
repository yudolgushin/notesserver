package com.erhn.ftknft.notesserver.utils.querybuilder.core


interface WhereQueryChain : QueryChain, OrderByQueryChain, LimitQueryChain {

    fun where(columnName: String, condition: CD, value: String): AndOrQueryChain

    fun wherePrep(columnName: String, condition: CD): AndOrQueryChain

}
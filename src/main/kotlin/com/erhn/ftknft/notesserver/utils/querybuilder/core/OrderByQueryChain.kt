package com.erhn.ftknft.notesserver.utils.querybuilder.core

interface OrderByQueryChain : QueryChain {
    fun orderBy(columnName: String, desc: DESC? = null): LimitQueryChain
}
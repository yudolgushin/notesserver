package com.erhn.ftknft.notesserver.utils.querybuilder.core.columnconstraint

import com.erhn.ftknft.notesserver.utils.querybuilder.core.QueryChain
import com.erhn.ftknft.notesserver.utils.querybuilder.core.ReferenceQueryChain
import com.erhn.ftknft.notesserver.utils.querybuilder.core.columnconstraint.EndInitsColumnsQueryChain

interface ForeignKeyQueryChain : QueryChain,
    EndInitsColumnsQueryChain {

    fun foreignKey(columnName: String): ReferenceQueryChain
}
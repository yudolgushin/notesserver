package com.erhn.ftknft.notesserver.utils.querybuilder

import com.erhn.ftknft.notesserver.utils.querybuilder.core.InitialQueryChain
import com.erhn.ftkfnt.notesserver.utils.querybuilder.imp.InitialQueryChainImp

object Qb {
    fun init(): InitialQueryChain = InitialQueryChainImp("")
}
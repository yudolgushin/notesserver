package com.erhn.ftknft.notesserver.utils.querybuilder.core.columnconstraint

import com.erhn.ftknft.notesserver.utils.querybuilder.core.QueryChain
import com.erhn.ftknft.notesserver.utils.querybuilder.core.columnconstraint.AllConstraintWithoutPrimaryQueryChain
import com.erhn.ftknft.notesserver.utils.querybuilder.core.columnconstraint.EndInitsColumnsQueryChain

interface PrimaryKeyQueryChain : QueryChain, EndInitsColumnsQueryChain {
    fun primaryKey(): AllConstraintWithoutPrimaryQueryChain
}
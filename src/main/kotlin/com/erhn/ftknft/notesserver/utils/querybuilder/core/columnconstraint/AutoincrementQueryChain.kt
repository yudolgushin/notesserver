package com.erhn.ftknft.notesserver.utils.querybuilder.core.columnconstraint

import com.erhn.ftknft.notesserver.utils.querybuilder.core.QueryChain

interface AutoincrementQueryChain : QueryChain,
    EndInitsColumnsQueryChain {

    fun autoincrement(): NotNullConstrainQueryChain
}
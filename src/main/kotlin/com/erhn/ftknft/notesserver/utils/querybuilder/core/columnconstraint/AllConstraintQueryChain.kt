package com.erhn.ftknft.notesserver.utils.querybuilder.core.columnconstraint

import com.erhn.ftknft.notesserver.utils.querybuilder.core.QueryChain

interface AllConstraintQueryChain : QueryChain,
    PrimaryKeyQueryChain,
    NotNullConstrainQueryChain,
    ForeignKeyQueryChain,
    DefaultConstraintQueryChain,
    ColumnQueryChain,
    EndInitsColumnsQueryChain,
    UniqueConstraintQueryChain {


}
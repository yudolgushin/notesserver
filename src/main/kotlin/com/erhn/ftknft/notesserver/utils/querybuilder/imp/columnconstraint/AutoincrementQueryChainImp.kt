package com.erhn.ftkfnt.notesserver.utils.querybuilder.imp.columnconstraint

import com.erhn.ftknft.notesserver.utils.querybuilder.core.columnconstraint.AutoincrementQueryChain
import com.erhn.ftknft.notesserver.utils.querybuilder.core.columnconstraint.NotNullConstrainQueryChain

class AutoincrementQueryChainImp(query: String) : BaseEndInitsQueryChain(query),
    AutoincrementQueryChain {

    override fun autoincrement(): NotNullConstrainQueryChain {
        query += " $AUTOINCREMENT"
        return NotNullConstraintQueryChainImp(query)
    }
}
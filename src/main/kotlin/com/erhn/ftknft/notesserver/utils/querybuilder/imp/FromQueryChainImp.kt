package com.erhn.ftkfnt.notesserver.utils.querybuilder.imp

import com.erhn.ftknft.notesserver.utils.querybuilder.imp.BaseQueryChain

class FromQueryChainImp(
    query: String,
    val limitQueryChain: com.erhn.ftknft.notesserver.utils.querybuilder.core.LimitQueryChain = LimitQueryChainImp(query, OrderByQueryChainImp(query))
) :
    BaseQueryChain(query), com.erhn.ftknft.notesserver.utils.querybuilder.core.FromQueryChain {
    override fun from(tableName: String): WhereQueryChainImp {
        query += " $FROM $tableName"
        return WhereQueryChainImp(query)
    }


    override fun limit(limit: Int): BaseQueryChain {
        return limitQueryChain.limit(limit)
    }

    override fun orderBy(columnName: String, desc: com.erhn.ftknft.notesserver.utils.querybuilder.core.DESC?): com.erhn.ftknft.notesserver.utils.querybuilder.core.LimitQueryChain {
        return limitQueryChain.orderBy(columnName, desc)
    }
}
package com.erhn.ftkfnt.notesserver.utils.querybuilder.imp

import com.erhn.ftknft.notesserver.utils.querybuilder.imp.BaseQueryChain
import com.erhn.ftknft.notesserver.utils.querybuilder.imp.SetAndWhereQueryChainImp

class SetQueryChainImp(query: String) : BaseQueryChain(query), com.erhn.ftknft.notesserver.utils.querybuilder.core.SetQueryChain {

    override fun set(columnName: String, value: String): com.erhn.ftknft.notesserver.utils.querybuilder.core.SetAndWhereQueryChain {
        if (query.contains(SET)) {
            query += ", $columnName ${com.erhn.ftknft.notesserver.utils.querybuilder.core.CD.EQ} $value"
        } else {
            query += " $SET $columnName ${com.erhn.ftknft.notesserver.utils.querybuilder.core.CD.EQ} $value"
        }
        return SetAndWhereQueryChainImp(query)
    }
}
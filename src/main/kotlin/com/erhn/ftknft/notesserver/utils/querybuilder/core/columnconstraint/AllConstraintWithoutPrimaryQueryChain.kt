package com.erhn.ftknft.notesserver.utils.querybuilder.core.columnconstraint

import com.erhn.ftknft.notesserver.utils.querybuilder.core.QueryChain

interface AllConstraintWithoutPrimaryQueryChain : QueryChain,
    ColumnQueryChain,
    DefaultConstraintQueryChain,
    AutoincrementQueryChain,
    UniqueConstraintQueryChain,
    NotNullConstrainQueryChain,
    EndInitsColumnsQueryChain {
}
package com.erhn.ftknft.notesserver.utils.querybuilder.core

interface FromQueryChain : QueryChain, LimitQueryChain {

    fun from(tableName: String): WhereQueryChain
}
package com.erhn.ftkfnt.notesserver.utils.querybuilder.imp.columnconstraint

import com.erhn.ftknft.notesserver.utils.querybuilder.core.columnconstraint.OnDeleteCascadeQueryChain
import com.erhn.ftknft.notesserver.utils.querybuilder.imp.BaseQueryChain
import com.erhn.ftknft.notesserver.utils.querybuilder.core.columnconstraint.ForeignKeyQueryChain

class OnDeleteCascadeQueryChainImp(val query: String, val foreignKeyQueryChain: ForeignKeyQueryChain) :
    OnDeleteCascadeQueryChain {

    override fun onDeleteCascade(): ForeignKeyQueryChain {
        val newQuery = "$query ON DELETE CASCADE"
        return ForeignKeyQueryChainImp(newQuery)
    }

    override fun build(): String {
        return foreignKeyQueryChain.build()
    }

    override fun foreignKey(columnName: String): com.erhn.ftknft.notesserver.utils.querybuilder.core.ReferenceQueryChain {
        return foreignKeyQueryChain.foreignKey(columnName)
    }

    override fun endInits(): BaseQueryChain {
        return foreignKeyQueryChain.endInits()
    }
}
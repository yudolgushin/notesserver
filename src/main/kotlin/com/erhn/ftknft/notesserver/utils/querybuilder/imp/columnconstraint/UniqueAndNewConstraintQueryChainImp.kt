package com.erhn.ftkfnt.notesserver.utils.querybuilder.imp.columnconstraint

import com.erhn.ftknft.notesserver.utils.querybuilder.core.columnconstraint.*

class UniqueAndNewConstraintQueryChainImp(
    query: String,
    private val uniqueConstraintQueryChain: UniqueConstraintQueryChain = UniqueConstraintQueryChainImp(query),
    private val columnQueryChain: ColumnQueryChain = ColumnQueryChainImp(query),
    private val foreignKeyQueryChain: ForeignKeyQueryChain = ForeignKeyQueryChainImp(query)
) : BaseEndInitsQueryChain(query), UniqueAndNewConstraintQueryChain {

    override fun unique(): ColumnQueryChain {
        return uniqueConstraintQueryChain.unique()
    }

    override fun column(columnName: String, type: Type): AllConstraintQueryChain {
        return columnQueryChain.column(columnName, type)
    }

    override fun foreignKey(columnName: String): com.erhn.ftknft.notesserver.utils.querybuilder.core.ReferenceQueryChain {
        return foreignKeyQueryChain.foreignKey(columnName)
    }
}

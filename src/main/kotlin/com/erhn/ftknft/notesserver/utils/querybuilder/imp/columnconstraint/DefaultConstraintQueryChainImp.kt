package com.erhn.ftkfnt.notesserver.utils.querybuilder.imp.columnconstraint

import com.erhn.ftknft.notesserver.utils.querybuilder.core.columnconstraint.ColumnQueryChain
import com.erhn.ftknft.notesserver.utils.querybuilder.core.columnconstraint.DefaultConstraintQueryChain
import com.erhn.ftknft.notesserver.utils.querybuilder.core.columnconstraint.ForeignKeyQueryChain

class DefaultConstraintQueryChainImp(
    query: String,
    private val foreignKeyQueryChain: ForeignKeyQueryChain = ForeignKeyQueryChainImp(query)
) : BaseEndInitsQueryChain(query), DefaultConstraintQueryChain {

    override fun default(value: String): ColumnQueryChain {
        query += " $DEFAULT $value"
        return ColumnQueryChainImp(query)
    }

    override fun foreignKey(columnName: String): com.erhn.ftknft.notesserver.utils.querybuilder.core.ReferenceQueryChain {
        return foreignKeyQueryChain.foreignKey(columnName)
    }
}
package com.erhn.ftknft.notesserver.utils.jwt.model

data class Jwt(
    val headers: Map<String, String>,
    val payloads: Map<String, Any>,
    val signature: String?
) {
    fun isSigned(): Boolean = signature != null
}
package com.erhn.ftknft.notesserver.utils.querybuilder.core

import com.erhn.ftknft.notesserver.utils.querybuilder.core.QueryChain
import com.erhn.ftknft.notesserver.utils.querybuilder.core.SetAndWhereQueryChain

interface SetQueryChain : QueryChain {

    fun set(columnName: String, value: String): SetAndWhereQueryChain
}
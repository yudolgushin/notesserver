package com.erhn.ftkfnt.notesserver.utils.querybuilder.imp.columnconstraint

import com.erhn.ftknft.notesserver.utils.querybuilder.core.columnconstraint.AllConstraintWithoutPrimaryQueryChain
import com.erhn.ftknft.notesserver.utils.querybuilder.core.columnconstraint.PrimaryKeyQueryChain

class PrimaryKeyQueryChainImp(query: String) : BaseEndInitsQueryChain(query),
    PrimaryKeyQueryChain {

    override fun primaryKey(): AllConstraintWithoutPrimaryQueryChain {
        query += " $PRIMARY_KEY"
        return AllConstraintWithoutPrimaryQueryChainImp(query)
    }
}
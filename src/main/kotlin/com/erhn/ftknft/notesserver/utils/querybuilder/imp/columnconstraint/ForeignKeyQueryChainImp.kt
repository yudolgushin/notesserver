package com.erhn.ftkfnt.notesserver.utils.querybuilder.imp.columnconstraint

import com.erhn.ftknft.notesserver.utils.querybuilder.core.ReferenceQueryChain
import com.erhn.ftknft.notesserver.utils.querybuilder.core.columnconstraint.ForeignKeyQueryChain

class ForeignKeyQueryChainImp(query: String) : BaseEndInitsQueryChain(query),
    ForeignKeyQueryChain {

    override fun foreignKey(columnName: String): com.erhn.ftknft.notesserver.utils.querybuilder.core.ReferenceQueryChain {
        query += ", $FOREIGN_KEY ($columnName)"
        return ReferenceQueryChainImp(query, this)
    }
}
package com.erhn.ftknft.notesserver

object Header {
    const val AUTHORIZATION = "Authorization"
    const val APPLICATION_JSON = "application/json"
    const val CONTENT_TYPE = "Content-type"
}
package com.erhn.ftknft.notesserver

import com.erhn.ftknft.notesserver.presentation.handlers.*
import io.vertx.core.AbstractVerticle
import io.vertx.ext.web.Router
import io.vertx.ext.web.handler.BodyHandler
import org.koin.core.KoinComponent
import org.koin.core.get

class MainVertical : AbstractVerticle(), KoinComponent {

    override fun start() {
        super.start()
        val router = Router.router(vertx)
        router.route().handler(BodyHandler.create())

        router.post("/${Path.REGISTER}").handler(get<RegisterHandler>())
        router.post("/${Path.AUTH}").handler(get<AuthHandler>())

        router.post("/${Path.REFRESH}").handler(get<RefreshTokensHandler>())

        router.post("/${Path.BOARDS}").handler(get<CreateBoardHandler>())
        router.get("/${Path.BOARDS}").handler(get<GetAllUserBoardsHandler>())
        router.delete("/${Path.BOARDS}/:${Param.BOARD_ID}").handler(get<DeleteBoardHandler>())

        router.delete("/${Path.BOARDS}/:${Param.BOARD_ID}/${Path.TASKS}/:${Param.TASK_ID}").handler(get<DeleteTaskHandler>())
        router.delete("/${Path.BOARDS}/:${Param.BOARD_ID}/${Path.TASKS}").handler(get<DeleteBoardTasksHandler>())
        router.put("/${Path.BOARDS}/:${Param.BOARD_ID}/${Path.TASKS}").handler(get<UpdateBoardHandler>())

        router.post("/${Path.BOARDS}/:${Param.BOARD_ID}/${Path.INVITES}").handler(get<AddToBoardHandler>())


        router.post("/${Path.BOARDS}/:${Param.BOARD_ID}/${Path.TASKS}").handler(get<CreateTasksHandler>())
        startServer(router)
    }

    private fun startServer(router: Router) {
        vertx.createHttpServer().requestHandler(router::handle).listen(PORT)
    }


    companion object {
        const val PORT = 5454
    }
}
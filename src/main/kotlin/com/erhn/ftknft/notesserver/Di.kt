package com.erhn.ftknft.notesserver

import com.erhn.ftknft.notesserver.data.Database
import com.erhn.ftknft.notesserver.data.repository.BoardRepositoryImpl
import com.erhn.ftknft.notesserver.data.repository.TaskRepositoryImpl
import com.erhn.ftknft.notesserver.data.repository.TokenRepositoryImpl
import com.erhn.ftknft.notesserver.data.repository.UserRepositoryImpl
import com.erhn.ftknft.notesserver.domain.repository.BoardRepository
import com.erhn.ftknft.notesserver.domain.repository.TaskRepository
import com.erhn.ftknft.notesserver.domain.repository.TokenRepository
import com.erhn.ftknft.notesserver.domain.repository.UserRepository
import com.erhn.ftknft.notesserver.domain.services.authuser.AuthUserService
import com.erhn.ftknft.notesserver.domain.services.authuser.AuthUserServiceImpl
import com.erhn.ftknft.notesserver.domain.services.checktoken.CheckTokenService
import com.erhn.ftknft.notesserver.domain.services.checktoken.CheckTokenServiceImpl
import com.erhn.ftknft.notesserver.domain.services.checkuser.CheckUserService
import com.erhn.ftknft.notesserver.domain.services.checkuser.CheckUserServiceImpl
import com.erhn.ftknft.notesserver.domain.services.checkuserboard.CheckBoardUserService
import com.erhn.ftknft.notesserver.domain.services.checkuserboard.CheckBoardUserServiceImpl
import com.erhn.ftknft.notesserver.domain.services.createboard.CreateBoardService
import com.erhn.ftknft.notesserver.domain.services.createboard.CreateBoardServiceImpl
import com.erhn.ftknft.notesserver.domain.services.createtasks.CreateTasksService
import com.erhn.ftknft.notesserver.domain.services.createtasks.CreateTasksServiceImpl
import com.erhn.ftknft.notesserver.domain.services.createtokens.CreateTokensService
import com.erhn.ftknft.notesserver.domain.services.createtokens.CreateTokensServiceImpl
import com.erhn.ftknft.notesserver.domain.services.createuser.CreateUserService
import com.erhn.ftknft.notesserver.domain.services.createuser.CreateUserServiceImpl
import com.erhn.ftknft.notesserver.domain.services.deletebooard.DeleteBoardService
import com.erhn.ftknft.notesserver.domain.services.deletebooard.DeleteBoardServiceImpl
import com.erhn.ftknft.notesserver.domain.services.deletebordertasks.DeleteBoardTasks
import com.erhn.ftknft.notesserver.domain.services.deletebordertasks.DeleteBoardTasksImpl
import com.erhn.ftknft.notesserver.domain.services.deletetask.DeleteTaskService
import com.erhn.ftknft.notesserver.domain.services.deletetask.DeleteTaskServiceImpl
import com.erhn.ftknft.notesserver.domain.services.getboard.GetBoardService
import com.erhn.ftknft.notesserver.domain.services.getboard.GetBoardServiceImpl
import com.erhn.ftknft.notesserver.domain.services.getuserboards.GetUserBoardsService
import com.erhn.ftknft.notesserver.domain.services.getuserboards.GetUserBoardsServiceImpl
import com.erhn.ftknft.notesserver.domain.services.inviteservice.InviteService
import com.erhn.ftknft.notesserver.domain.services.inviteservice.InviteServiceImpl
import com.erhn.ftknft.notesserver.domain.services.refreshtokens.FindRefreshTokenService
import com.erhn.ftknft.notesserver.domain.services.refreshtokens.FindRefreshTokenServiceImpl
import com.erhn.ftknft.notesserver.domain.services.updateboard.UpdateBoardService
import com.erhn.ftknft.notesserver.domain.services.updateboard.UpdateBoardServiceImpl
import com.erhn.ftknft.notesserver.domain.services.updatetasks.UpdateTasksService
import com.erhn.ftknft.notesserver.domain.services.updatetasks.UpdateTasksServiceImpl
import com.erhn.ftknft.notesserver.presentation.handlers.*
import com.google.gson.Gson
import org.koin.dsl.module

val dataModule = module {
    single { Database() }
    single { get<Database>().userDao }
    single { get<Database>().refreshTokenDao }
    single { get<Database>().boardDao }
    single { get<Database>().taskDao }
    single { get<Database>().boardUserDao }
    factory<UserRepository> { UserRepositoryImpl(userDao = get()) }
    factory<TokenRepository> { TokenRepositoryImpl(refreshTokenDao = get()) }
    factory<BoardRepository> { BoardRepositoryImpl(boardDao = get(), boardUserDao = get()) }
    factory<TaskRepository> { TaskRepositoryImpl(taskDao = get(), boardDao = get()) }
}

val utilsModule = module {
    factory { Gson() }
}

val domainModule = module {
    factory<CreateUserService> { CreateUserServiceImpl(userRepository = get()) }
    factory<AuthUserService> { AuthUserServiceImpl(userRepository = get()) }
    factory<CreateTokensService> { CreateTokensServiceImpl(tokenRepository = get()) }
    factory<FindRefreshTokenService> { FindRefreshTokenServiceImpl(tokenRepository = get(), userRepository = get()) }
    factory<CheckTokenService> { CheckTokenServiceImpl(userRepository = get()) }
    factory<CreateBoardService> { CreateBoardServiceImpl(boardRepository = get()) }
    factory<CheckBoardUserService> { CheckBoardUserServiceImpl(boardRepository = get()) }
    factory<CreateTasksService> { CreateTasksServiceImpl(taskRepository = get()) }
    factory<GetUserBoardsService> { GetUserBoardsServiceImpl(boardRepository = get(), taskRepository = get()) }
    factory<DeleteBoardService> { DeleteBoardServiceImpl(checkBoardUserService = get(), boardRepository = get()) }
    factory<DeleteTaskService> { DeleteTaskServiceImpl(checkBoardUserService = get(), taskRepository = get()) }
    factory<DeleteBoardTasks> { DeleteBoardTasksImpl(checkBoardUserService = get(), taskRepository = get()) }
    factory<UpdateTasksService> { UpdateTasksServiceImpl(taskRepository = get()) }
    factory<GetBoardService> { GetBoardServiceImpl(taskRepository = get(), boardRepository = get()) }
    factory<UpdateBoardService> {
        UpdateBoardServiceImpl(
            checkBoardUserService = get(),
            updateTasksService = get(),
            getBoardService = get(),
            boardRepository = get()
        )
    }
    factory<CheckUserService> { CheckUserServiceImpl(userRepository = get()) }
    factory<InviteService> { InviteServiceImpl(checkBoardUserService = get(), boardsRepository = get()) }
}

val controlersModule = module {
    factory { RegisterHandler(gson = get(), createUserService = get(), createTokensService = get()) }
    factory { AuthHandler(gson = get(), authUserService = get(), createTokensService = get()) }
    factory { RefreshTokensHandler(gson = get(), findRefreshTokenService = get(), createTokensService = get()) }
    factory { CreateBoardHandler(gson = get(), checkTokenService = get(), createBoardService = get()) }
    factory { CreateTasksHandler(gson = get(), checkTokenService = get(), boardUserService = get(), createTasksService = get()) }
    factory { GetAllUserBoardsHandler(gson = get(), checkTokenService = get(), getUserBoardsService = get()) }
    factory { DeleteBoardHandler(gson = get(), checkTokenService = get(), deleteBoardService = get()) }
    factory { DeleteTaskHandler(gson = get(), checkTokenService = get(), deleteTaskService = get()) }
    factory { DeleteBoardTasksHandler(gson = get(), checkTokenService = get(), deleteBoardTasks = get()) }
    factory { UpdateBoardHandler(gson = get(), checkTokenService = get(), updateBoardService = get()) }
    factory { AddToBoardHandler(gson = get(), checkTokenService = get(), inviteService = get()) }
}
